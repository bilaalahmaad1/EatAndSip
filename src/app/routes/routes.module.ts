import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ManageCountryComponent } from '../admin/manage-address/manage-country/manage-country.component';
import { ManageCityComponent } from '../admin/manage-address/manage-city/manage-city.component';
import { ManageAreaComponent } from '../admin/manage-address/manage-area/manage-area.component';
import { AddCountryComponent } from '../admin/manage-address/manage-country/add-country/add-country.component';
import { UpdateCountryComponent } from '../admin/manage-address/manage-country/update-country/update-country.component';
import { CuisinesComponent } from '../admin/manage-cuisines/cuisines/cuisines.component';
import { CategoryComponent } from '../admin/manage-category/category/category.component';
import { SubCategoryComponent } from '../admin/manage-category/sub-category/sub-category.component';
import { IngredientListComponent } from '../admin/manage-ingredient/ingredient-list/ingredient-list.component'
import { IngredientTypeComponent } from '../admin/manage-ingredient/ingredient-type/ingredient-type.component';
import { VendorDetailComponent } from '../admin/manage-vendor/vendor-detail/vendor-detail.component';
import { AdminComponent } from '../admin/admin.component';
import { LoginComponent } from '../login/login.component';
import { VendorComponent } from '../vendor/vendor.component';
import { ManageItemsComponent } from '../vendor/manage-items/manage-items.component';
import { ProfileComponent } from '../profile/profile.component';
import { ManageOrderComponent } from '../vendor/manage-order/manage-order.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },


  {
    path: 'admin', component: AdminComponent,
    children: [
      { path: 'Country', component: ManageCountryComponent, outlet: 'myOutlet' },
      { path: 'City', component: ManageCityComponent, outlet: 'myOutlet' },
      { path: 'Area', component: ManageAreaComponent, outlet: 'myOutlet' },
      { path: 'Cuisines', component: CuisinesComponent, outlet: 'myOutlet' },
      { path: 'Categories', component: CategoryComponent, outlet: 'myOutlet' },
      { path: 'SubCategories', component: SubCategoryComponent, outlet: 'myOutlet' },
      { path: 'ingredientTypes', component: IngredientTypeComponent, outlet: 'myOutlet' },
      { path: 'ingredientList', component: IngredientListComponent, outlet: 'myOutlet' },
      { path: 'VendorDetails', component: VendorDetailComponent, outlet: 'myOutlet' },
      { path: 'profile', component: ProfileComponent, outlet: 'myOutlet' },
      { path: 'items', component: ManageItemsComponent, outlet: 'myOutlet' },
      { path: 'orders', component:ManageOrderComponent , outlet: 'myOutlet' },
    ]
  },
  {
    path: 'vendor', component: VendorComponent,
    children: [
      { path: 'ingredientTypes', component: IngredientTypeComponent, outlet: 'vendorOutlet' },
      { path: 'ingredientList', component: IngredientListComponent, outlet: 'vendorOutlet' },
      { path: 'items', component: ManageItemsComponent, outlet: 'vendorOutlet' },
      { path: 'orders', component:ManageOrderComponent , outlet: 'vendorOutlet' },
      { path: 'profile', component: ProfileComponent, outlet: 'vendorOutlet' },
    ]
  },
  { path: 'login', component: LoginComponent },

  { path: 'logout', component: LoginComponent }


];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),


  ],
  exports: [RouterModule],
  declarations: []
})
export class RoutesModule { }
