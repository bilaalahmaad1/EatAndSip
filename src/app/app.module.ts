import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RoutesModule } from './routes/routes.module';
import { AdminModule } from './admin/admin.module';
import { LoginModule } from './login/login.module';
import { FrontEndRouteModule } from '../app/user/front-end-route/front-end-route.module';
import { UserModule } from '../app/user/user.module';
import { VendorModule } from './vendor/vendor.module';

// import {FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    AppComponent,


  ],
  imports: [
    BrowserModule,
    AdminModule,
    VendorModule,
    UserModule,
    LoginModule,
    RoutesModule,
    FrontEndRouteModule,
    // ReactiveFormsModule,
    // FormsModule

  ],
  exports: [],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
