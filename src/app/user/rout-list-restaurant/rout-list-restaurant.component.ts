import { Component, OnInit , Input } from '@angular/core';
import {order} from '../restaurant-menu/restaurant-menu.component';

@Component({
  selector: 'app-rout-list-restaurant',
  templateUrl: './rout-list-restaurant.component.html',
  styleUrls: ['./rout-list-restaurant.component.css']
})
export class RoutListRestaurantComponent implements OnInit {
  
  order: order;
  value;
  orderDetail:order[]=[];
  revieworder:boolean=false
  @Input() currentVendor;
  constructor() { }
  onCount(cat) {
    this.value = cat;
  }
  ngOnInit() {
    
  }
  addToCart(orders){
    // console.log("order");
    console.log(orders);
  
    this.order= new order();
    this.order.itemName=""
    this.order.itemPrice=""
    this.order.itemName=orders.itemName;
    this.order.itemPrice=orders.itemPrice;
    this.orderDetail.push(this.order)

   
  //  console.log("orderDetail");
  
   console.log(this.orderDetail);

  }
  reviewOrder(){
    this.revieworder=true
  }

}
