import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { NgModule, ViewChild } from '@angular/core';
import { UserCityService, UserCity } from '../services/userCity.service';
import{UserAreaService,Area} from '../services/userArea.service';
import { Ng2AutoComplete, Ng2AutoCompleteComponent, Ng2AutoCompleteDirective } from 'ng2-auto-complete';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

class DropDown {
  
    constructor(public id: number,
      public itemName: string) { }
  }

@Component({
  selector: 'app-search-filter',
  templateUrl: './search-filter.component.html',
  styleUrls: ['./search-filter.component.css']
})
export class SearchFilterComponent implements OnInit {
  location :string;
  @Output() searchLocation: EventEmitter<string> = new EventEmitter<string>();
  tempareas: Area[] = [];
  areas: Area[] = [];
  tempcities: UserCity[] = [];
  cities: UserCity[]=[];
  area: string[];
  myForm: FormGroup;

  selectedCityItems = [];
  dropdownCitySettings = {};
  dropdownCityObject: DropDown;
  dropdownCityList: DropDown[] = []

  selectedAreaItems = [];
  dropdownAreaSettings = {};
  dropdownAreaObject: DropDown;
  dropdownAreaList: DropDown[] = []

  constructor(private CityServices: UserCityService, 
    private fb: FormBuilder
     , public router:Router,
    public areaServce:UserAreaService) {
    this.myForm = this.fb.group({
      city: [null, Validators.required],
      area: [null, Validators.required]
    });
    
  }



  ngOnInit() {
    this.CityServices.getCities()
      .subscribe(response => {
        this.tempcities=response; 
        console.log(this.tempcities);
        for(let i =0;i<this.tempcities.length;i++){
          if(this.tempcities[i].cityStatus==="Active"){
            this.cities.push(this.tempcities[i]);
          }
        }
        for (let j = 0; j < this.cities.length; j++) {
          // console.log("i am in city service loop");
          this.dropdownCityObject = new DropDown(j, this.cities[j].name)
          this.dropdownCityList.push(this.dropdownCityObject)
        }
      });
      this.areaServce.getAreas()
      .subscribe(response => {
        this.tempareas=response; 
        for(let i =0;i<this.tempareas.length;i++){
          if(this.tempareas[i].areaStatus==="Active"){
            this.areas.push(this.tempareas[i]);
          }
        }
     
      });
      this.dropdownCitySettings = {
        singleSelection: true,
        text: "Select City",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      };
      this.dropdownAreaSettings = {
        singleSelection: true,
        text: "Select Area",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      };
      this.selectedCityItems = [];
      this.selectedAreaItems = [];
      // console.log(this.cities)
  }
  OnItemDeSelect(item: any) {
    console.log(item);


    if (this.selectedCityItems.length < 1) {
      this.dropdownAreaList = [];
      this.selectedAreaItems = [];
      
    }

    // console.log(this.selectedItems2);
  }
  onItemSelect(item: any) {
    console.log(item);
    console.log("selectedCategoriesItems");
    // console.log(this.selectedCategoriesItems);

    if (this.selectedCityItems.length > 0) {
      
      console.log(this.areas)
      this.dropdownAreaList = [];
      
      
      for (let j = 0; j < this.areas.length; j++) {
       

        if (this.areas[j].cityName === this.selectedCityItems[0].itemName) {
         
          this.dropdownAreaObject = new DropDown(j, this.areas[j].name)
          this.dropdownAreaList.push(this.dropdownAreaObject)
          
         
        
        }
        if (item.itemName === this.selectedCityItems[0].itemName) {
         

          this.selectedAreaItems = []
     
          
        }

      }



    }
  }
  resturants(){
    let area=this.selectedAreaItems[0].itemName
    let city=this.selectedCityItems[0].itemName
     this.location =area+","+city;
    console.log(this.location);
    this.searchLocation.emit(this.location)
    
    this.router.navigate(['/restaurants']);
     }
    
}


  // select(city) {
  //   this.area.splice(0,this.area.length)
  //   // this.cityvalue=city
  //   for (let i = 0; i < this.cities.length; i++) {

  //     if (city === this.cities[i].city) {
  //       this.area[i] = this.cities[i].area;
  //       console.log(this.area[i]);
  //     }
  //   }
  // }

// }
  
