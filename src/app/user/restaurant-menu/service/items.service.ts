import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { HttpClient } from '../../../shared/common/httpClient';
import { Headers, RequestOptions } from '@angular/http';
export class ItemUser {
    public _id: number;

    public category:string
    public cuisine:string
    public subCategory:string
    public itemName:string;
    public itemPricePerUnit:string;
    public itemStatus:string;
    public ingredientListNames:string[];
    public ingredientListPrices:string[];
    public listIngredienttype:string
   public vendorName:string;
   public approval:string;
   public  listIngredientName:string;
    
}
@Injectable()
export class itemService {
    
        constructor(private http: Http, private HttpClient: HttpClient) {
        }
    
      
    
        getItemList() {
    
       
            let url = "itemsusers";
        
            return this.HttpClient
                .get(url)
                .map((response: Response) => {
                    let data = response.json();
                    return data;
                });
        }
       
        private handlError(error: Response) {
            let msg = `Error status code${error.status} at ${error.url}`;
            return Observable.throw(msg);
        }
    }