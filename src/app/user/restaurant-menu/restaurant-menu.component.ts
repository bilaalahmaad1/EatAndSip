import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import {itemService,ItemUser} from './service/items.service';
 export class order{
 public  itemName:string
 public  itemPrice:string

}
@Component({
  selector: 'app-restaurant-menu',
  templateUrl: './restaurant-menu.component.html',
  styleUrls: ['./restaurant-menu.component.css']
})
export class RestaurantMenuComponent implements OnInit {

  temporders: order[] = [];
  order:order;
  
  counter = 0;
  @Output() count: EventEmitter<number> = new EventEmitter<number>();
  @Output() orders: EventEmitter<order> = new EventEmitter<order>();
  @Input ()currentVendor;
  items :ItemUser[]=[]
  currentVendoritems :ItemUser[]=[]
  constructor(public itemService:itemService) { 
   
  
  
  }
 
  ngOnInit() {
    console.log("in menue detail");
    console.log(this.currentVendor);
    this.itemService.getItemList().subscribe(res=>{ this.items=res
      let j=0;
        for(let i =0;i<this.items.length;i++){
            if(this.currentVendor.vendorName===this.items[i].vendorName){
                this.currentVendoritems[j]=this.items[i]
                j++;

            }
        } 
        console.log( this.currentVendoritems);

    });
  }
  plusclick(item:ItemUser) {
    this.order= new order();
    this.order.itemName=""
    this.order.itemPrice=""
    this.order.itemName=item.itemName;
    this.order.itemPrice=item.itemPricePerUnit;
    // this.temporders.push(this.order)
    this.orders.emit(this.order);
   
    this.counter++;
    this.count.emit(this.counter);
    console.log(this.counter);
  }

}
