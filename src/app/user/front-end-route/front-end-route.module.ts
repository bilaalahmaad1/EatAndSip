import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RestaurantsComponent} from '../restaurants/restaurants.component';
import { Routes, RouterModule } from '@angular/router';

import {ReviewOrderComponent} from '../review-order/review-order.component';
import {CheckoutComponent} from '../checkout/checkout.component';
import {SuccessPageComponent} from '../success-page/success-page.component';
const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  //  { path: 'restaurants/:resturant', component: RestaurantsComponent },
  { path: 'restaurants', component: RestaurantsComponent },

  { path: 'review-order', component: ReviewOrderComponent },
  { path: 'checkout', component: CheckoutComponent },
  { path: 'success', component: SuccessPageComponent }

];

@NgModule({
  imports: [
    CommonModule,
   RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule],
  declarations: []
})
export class FrontEndRouteModule { }
