import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeHeaderComponent } from './home-header/home-header.component';
import { LayoutComponent } from './layout/layout.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { SearchFilterComponent } from './search-filter/search-filter.component';
import { GoodFoodComponent } from './good-food/good-food.component';
import { RestaurantsComponent } from './restaurants/restaurants.component';
import { RouterModule } from '@angular/router';
import { ClearFilterComponent } from './clear-filter/clear-filter.component';
import { RouteListComponent } from './route-list/route-list.component';
import { RestaurantsListItemsComponent } from './restaurants-list-items/restaurants-list-items.component';
import { RestaurantItemsComponent } from './restaurant-items/restaurant-items.component';
import { RoutListRestaurantComponent } from './rout-list-restaurant/rout-list-restaurant.component';
import { RestaurantMenuComponent } from './restaurant-menu/restaurant-menu.component';
import { ReviewOrderComponent } from './review-order/review-order.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { SuccessPageComponent } from './success-page/success-page.component';
import { UserCityService } from './services/userCity.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from '../../../node_modules/angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { UserAreaService } from './services/userArea.service';
import {vendorDetailService} from './restaurants-list-items/service/restaurant.list.service';
import {itemService} from './restaurant-menu/service/items.service';
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMultiSelectModule
  ],
  declarations: [
    HomeHeaderComponent,
    LayoutComponent,
    NavigationBarComponent,
    SearchFilterComponent,
    GoodFoodComponent,
    RestaurantsComponent,
    ClearFilterComponent,
    RouteListComponent,
    RestaurantsListItemsComponent,
    RestaurantItemsComponent,
    RoutListRestaurantComponent,
    RestaurantMenuComponent,
    ReviewOrderComponent,
    CheckoutComponent,
    SuccessPageComponent
  ],
  exports: [LayoutComponent],
  providers: [UserCityService, UserAreaService,vendorDetailService,itemService],
})
export class UserModule { }
