import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-route-list',
  templateUrl: './route-list.component.html',
  styleUrls: ['./route-list.component.css']
})
export class RouteListComponent implements OnInit {

  url: any;

   constructor(private router: Router) {

   }

   ngOnInit() {

     this.router.events.subscribe((res) => {

       this.url = this.router.url;
       console.log(this.url);
     });

}
}
