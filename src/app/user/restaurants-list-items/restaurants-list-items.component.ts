import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import {VendorDetail,vendorDetailService} from './service/restaurant.list.service';

@Component({
  selector: 'app-restaurants-list-items',
  templateUrl: './restaurants-list-items.component.html',
  styleUrls: ['./restaurants-list-items.component.css']
})
export class RestaurantsListItemsComponent implements OnInit {
  
  url: string;
  showrestaurant: boolean = false;
  resturants: string[] = [];
  @Input() vendors;
  currentrestaurant: VendorDetail;
  constructor( public router:Router) {
    this.router.events.subscribe((res) => {
      
            this.url = this.router.url;
            console.log(this.url);
          });

   }

  ngOnInit() {

  }
  restaurant(r:VendorDetail){
  this.currentrestaurant=r;
    this.showrestaurant=true;
  

  }

}
