import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpClient } from '../../../shared/common/httpClient';
import 'rxjs/Rx';

export class Vendors {
    public _id: number;
    public vendorName: string;
    public email:string
    public password:string
    public confirmPassword:string
    public userName:string
    public vendorSMSnumber:string
    public contactAddress:string
    public vendorCallNumber:string
    public categories:string[];
    public cusines:string[];
    public city:string
    public area:string
    public deliveryLocation:string[];
    public commision:number
    public serviceTax:string
    public minOrderValue:string
    public deliveryTime:string
    public paymentOption:string
    public resturantType:string
    public deliveryCostAmount:string
    public deliveryCostKM:string
    public deliveryAdditionalCostKM:string
    public vendorStatus:string;
    public type:string;

}

@Injectable()
export class UserVendorService {
    constructor(private HttpClient: HttpClient) { }
    getVendors() {


        let url = "uservendors";

        return this.HttpClient
            .get(url)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
}
