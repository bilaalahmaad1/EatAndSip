import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {vendorDetailService,VendorDetail} from '../restaurants-list-items/service/restaurant.list.service';

@Component({
  selector: 'app-home-header',
  templateUrl: './home-header.component.html',
  styleUrls: ['./home-header.component.css']
})
export class HomeHeaderComponent implements OnInit {
 temp: VendorDetail[]=[];
 vendors: VendorDetail[]=[];
  url: any;
  Location:string[]=[];
  showrestaurants:boolean=false;
  constructor(private router: Router,private vendorDetailService:vendorDetailService ) {

  }
  ngOnInit() {
    
    this.router.events.subscribe((res) => {
      
            this.url = this.router.url;
            console.log(this.url);
          });

  
  }
  location(location:string){
    this.Location=location.split(',');
    let j=0
    // console.log(this.Location[0]);
    // console.log(this.Location[1]);
    this.vendorDetailService.getVendorDetailList().subscribe(res=>{this.temp=res
   
      for(let i =0;i<this.temp.length;i++){
        
        if (this.temp[i].area===this.Location[0] && this.temp[i].city===this.Location[1])
        {  
         this.vendors[j]=this.temp[i];
         j++;
        }
       
      } 
      this.showrestaurants=true
      console.log(this.vendors)
    
    });
    
    
    

  }
}
