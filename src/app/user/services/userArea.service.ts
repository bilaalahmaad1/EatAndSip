import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpClient } from '../../shared/common/httpClient';
import 'rxjs/Rx';

export class Area {
    public _id: number;
    public name: string;
    public countryName: string;
    public cityName: string;
    public areaStatus: string;

}

@Injectable()
export class UserAreaService {
    constructor(private HttpClient: HttpClient) { }
    getAreas() {


        let url = "userareas";

        return this.HttpClient
            .get(url)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
}
