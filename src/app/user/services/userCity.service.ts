import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import {HttpClient} from '../../shared/common/httpClient';
import 'rxjs/Rx';

export class UserCity {
  public _id: number; 
  public name: string; 
  public countryName: string
  public  cityStatus:string;

}

@Injectable()
export class UserCityService {
  constructor(private HttpClient: HttpClient) { }
  getCities() {
    
           
              let url = "usercities";
                
                 return this.HttpClient
                .get(url)
                .map((response: Response) => {
                    let data = response.json();
                    return data;
                });
        }
}
