import { Component, OnInit, ViewChild, ElementRef, OnChanges, OnDestroy, AfterViewInit } from '@angular/core';
import { Order, orderService } from './services/order.service';
import { profileService } from '../../profile/service/profile.service';

@Component({
  selector: 'app-manage-order',
  templateUrl: './manage-order.component.html',
  styleUrls: ['./manage-order.component.css']
})
export class ManageOrderComponent implements OnInit, OnChanges,OnDestroy,AfterViewInit{
  adminorders: Order[];
  temporders: Order[];
  vendorName: string;
  vendor: boolean;
  admin: boolean;
  orders: Order[];
  @ViewChild('myselect') public select: ElementRef;
 
  constructor(public orderService: orderService,
    public profileService: profileService) { 
      this.orders=[];
     
    }
    ngAfterViewInit(){
      console.log("ngAfterViewInit"); 
    }
    ngOnChanges(){
      console.log("ngOnChanges"); 
    }
    ngOnDestroy(){
      console.log("ngOnDestroy"); 
    }
    
      ngOnInit() {

    this.profileService.getUser().subscribe(res => {
      if (res.user.vendor) {
        this.vendor = true
        this.vendorName = res.user.vendor.vendorName;
        this.getOrders(this.vendorName);
      } else {
        this.admin = true
        this.orderService.getOrderList().subscribe(res => {this.adminorders=res
          console.log(this.adminorders); });
        
      }
    });
  }

  getOrders(user) {
    this.orderService.getOrderList().subscribe(res => {
    this.temporders = res
      let index: number = 0
      for (let i = 0; i < this.temporders.length; i++) {
        if (this.temporders[i].shopName === user) {
          this.orders[index] = this.temporders[i];
          index++
        }
      }
      console.log(this.orders); 
    });

  }
  getPaymentStatus(order){
      console.log(order)
      this.orderService.updateOrder(order._id,order).subscribe(res=>{console.log(res) });
  }
  getDeliveryStatus(order){
  
      this.orderService.updateOrder(order._id,order).subscribe(res=>{console.log(res) });
      // this.getOrders(order.shopName);
      // this.ngOnInit();
     
     
  }
 
  
  
  

}
