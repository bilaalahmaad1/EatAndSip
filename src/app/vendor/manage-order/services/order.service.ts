import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { HttpClient } from '../../../shared/common/httpClient';
import { Headers, RequestOptions } from '@angular/http';
export class Order {
   
    public _id: number;
    public shopName: string
    public customerName: string
    public orderTotal: string
    public paymentStatus: string;
    public dileveryStatus: string;


}
@Injectable()
export class orderService {

    constructor(private http: Http, private HttpClient: HttpClient) {
    }

    getOrder(id: number) {
        return this.getOrderList()
            .map(Orders => Orders.find(Order => Order.id === id))
            // .do(value => (console.log(value)))
            .catch(this.handlError);
    }

    getOrderList() {

        // return this.http
        //     .get('assets/city.json')
        //     .map((response: Response) => <City[]>response.json().City)
        //     // .do(value => console.log(value))
        //     .catch(this.handlError);
        let url = "orders";
        // console.log("sbvhk")
        return this.HttpClient
            .get(url)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
    postOrder(order: Order) {
        let url = "orders"
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .post(url, order)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
    updateOrder(id, order: Order) {
        let url = "orders/" + id
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .put(url, order)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
    deleteOrder(id) {
        console.log(id)
        let url = "orders/" + id
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .delete(url)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }

    private handlError(error: Response) {
        let msg = `Error status code${error.status} at ${error.url}`;
        return Observable.throw(msg);
    }
}