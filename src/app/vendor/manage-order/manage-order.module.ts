import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageOrderComponent } from './manage-order.component';
import {orderService} from './services/order.service'; 
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    BrowserModule,
    FormsModule,
  ],
  declarations: [ManageOrderComponent],
  providers:[orderService],
  exports:[ManageOrderComponent]
})
export class ManageOrderModule { }
