import { Component, OnInit } from '@angular/core';
import {ManageIngredientComponent} from '../admin/manage-ingredient/manage-ingredient/manage-ingredient.component';

@Component({
  selector: 'app-vendor',
  templateUrl: './vendor.component.html',
  styleUrls: ['./vendor.component.css']
})
export class VendorComponent implements OnInit {

  constructor(public ManageIngredientComponent:ManageIngredientComponent) { }

  ngOnInit() {
    console.log("in vendor ")
   this.ManageIngredientComponent.ngOnInit();
    
  }

}
