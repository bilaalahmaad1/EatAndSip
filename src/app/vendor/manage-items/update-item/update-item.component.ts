import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { Item, itemService } from '../services/item.service';
import { Category, categoryService } from '../../../admin/manage-category/services/category.services';
import { subCategory, subcategoryService } from '../../../admin/manage-category/services/subCategory.service';
import { Cuisine, cuisineService } from '../../../admin/manage-cuisines/services/cuisine.service';
import { IngredientList, ingredientListService } from '../../../admin/manage-ingredient/services/ingredientlist.service';
import { IngredientType, ingredienttypeService } from '../../../admin/manage-ingredient/services/ingredienttype.service';
import { profileService } from '../../../profile/service/profile.service';
import { VendorDetail, vendorDetailService } from '../../../admin/manage-vendor/services/vendorDetail.service'


class DropDown {

  constructor(public id: number,
    public itemName: string) { }
}
class listnameAndtype {

  public name: string = ""
  public type: string = ""

}
class namesoflist {
  public name: string = "";
  public price: string = "";
}
// class selectedingredientlist {
//   public name: string = "";
// }
@Component({
  selector: 'app-update-item',
  templateUrl: './update-item.component.html',
  styleUrls: ['./update-item.component.css']
})
export class UpdateItemComponent implements OnInit {
  vendors: VendorDetail[] = [];
  vendorName: string;
  admin: boolean;
  listsplit: string;
  listnames: listnameAndtype[] = []
  listname: listnameAndtype;
  // selectedingredientlist: string;
  ingredientLists: any;
  showlist: boolean;
  showListOfIngredients: boolean = false;
  ingredientlists: namesoflist[] = [];
  ingredientlist: namesoflist;
  @Input() updateId: number;
  @Input() updateItems: Item[];
  @Output() isBack: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() updatedItems: EventEmitter<Item[]> = new EventEmitter<Item[]>();

  items: Item[] = [];
  item: Item;

  tempcategories: any;
  categories: Category[] = [];
  selectedCategoriesItems = [];
  dropdownCategoriesSettings = {};
  dropdownCategoriesObject: DropDown;
  dropdownCategoriesList: DropDown[] = []

  tempsubcategories: any;
  subcategories: subCategory[] = [];
  selectedSubCategoriesItems = [];
  dropdownSubCategoriesSettings = {};
  dropdownSubCategoriesObject: DropDown;
  dropdownSubCategoriesList: DropDown[] = []

  tempcusines: any;
  cusines: Cuisine[] = [];
  selectedCusinesItems = [];
  dropdownCusinesSettings = {};
  dropdownCusinesObject: DropDown;
  dropdownCusinesList: DropDown[] = []
  @ViewChild('myRadioyes') public radioyes: ElementRef;
  @ViewChild('myRadiono') public radiono: ElementRef;
  constructor(public itemService: itemService, public categoryService: categoryService,
    public cuisineService: cuisineService,
    public subcategoryService: subcategoryService,
    public profileService: profileService,
    public vendorDetailService: vendorDetailService,
    public ingredienttypeService: ingredienttypeService,
    public ingredientListService: ingredientListService) {
    // this.selectedingredientlist= new selectedingredientlist();
  }

  ngOnInit() {
    this.vendorDetailService.getVendorDetailList().subscribe(res => { this.vendors = res });
    this.profileService.getUser().subscribe(res => {
      if (res.user.vendor) {
        this.vendorName = res.user.vendor.vendorName;
      }
      if (res.user.admin) {
        this.admin = true
      }
    });
    this.ingredientListService.getIngredientListList().subscribe(res => {
      this.ingredientLists = res
      console.log(this.ingredientLists)

      let index: number = 0;
      for (let i = 0; i < this.ingredientLists.length; i++) {
        if (this.ingredientLists[i].IngredientListStatus === "Active") {
          this.listname = new listnameAndtype();
          this.listname.name = this.ingredientLists[i].names.join("/");
          this.listname.type = this.ingredientLists[i].IngredientType
          // console.log(this.item.listIngredienttype)
          // if (this.item.listIngredienttype === this.ingredientLists[i].IngredientType) {
          //    this.selectedingredientlist = 'bilal'//isuing
          // }
          this.listnames.push(this.listname)
          index++;
        }
      }
    });
    this.itemService.getItemList()
      .subscribe(res => {
        this.items = res
        this.radiono.nativeElement.checked = true;
        for (let k = 0; k < this.items.length; k++) {

          if (this.items[k].itemName === this.item.itemName) {
            for (let g = 0; g < this.items[k].ingredientListNames.length; g++) {
              this.radioyes.nativeElement.checked = true
              this.showListOfIngredients = true;
              this.showlist = true;
              this.radiono.nativeElement.checked = false;
              // this.nolistofingredient=false


              this.ingredientlist = new namesoflist();
              this.ingredientlist.name = this.items[k].ingredientListNames[g];
              this.ingredientlist.price = this.items[k].ingredientListPrices[g];
              this.ingredientlists.push(this.ingredientlist);
              console.log(this.ingredientlist)

            }
          }




        }


      });
    for (let i = 0; i < this.updateItems.length; i++) {
      if (this.updateItems[i]._id === this.updateId) {
        this.item = this.updateItems[i];
        console.log(this.item)
      }
    }
    this.categoryService.getCategoryList()
      .subscribe(res => {
        this.tempcategories = res;
        for (let i = 0; i < this.tempcategories.length; i++) {
          if (this.tempcategories[i].categoryStatus === "Active") {
            this.categories.push(this.tempcategories[i]);
          }
        }
        for (let i = 0; i < this.categories.length; i++) {

          this.dropdownCategoriesObject = new DropDown(i, this.categories[i].name)
          this.dropdownCategoriesList.push(this.dropdownCategoriesObject)
        }
       
       
          
                    for (let j = 0; j < this.dropdownCategoriesList.length; j++) {
          
                      if (this.item.category === this.dropdownCategoriesList[j].itemName) {
                        
                        // this.dropcategory=true;
                         this.selectedCategoriesItems[0] = new DropDown(j, this.dropdownCategoriesList[j].itemName);
                     
                      }
                    
                }
      });
    this.cuisineService.getCuisineList()
      .subscribe(res => {
        // console.log("i am in cusine service");
        this.tempcusines = res;
        for (let i = 0; i < this.tempcusines.length; i++) {
          if (this.tempcusines[i].cusineStatus === "Active") {
            this.cusines.push(this.tempcusines[i]);
          }
        }


        console.log(this.cusines);
        for (let i = 0; i < this.cusines.length; i++) {
          // console.log("i am in cusine service lop");
          this.dropdownCusinesObject = new DropDown(i, this.cusines[i].name)
          this.dropdownCusinesList.push(this.dropdownCusinesObject)
        }
        for (let j = 0; j < this.dropdownCusinesList.length; j++) {
          
                      if (this.item.cuisine === this.dropdownCusinesList[j].itemName) {
                        
                        // this.dropcategory=true;
                         this.selectedCusinesItems[0] = new DropDown(j, this.dropdownCusinesList[j].itemName);
                     
                      }
                    
                }
              });

        this.subcategoryService.getsubCategoryList()
          .subscribe(res => {
            console.log("sub category");
            console.log(res);

            this.tempsubcategories = res;
            for (let i = 0; i < this.tempsubcategories.length; i++) {
              if (this.tempsubcategories[i].sucCategoryStatus === "Active") {
                this.subcategories.push(this.tempsubcategories[i]);
              }
            }
            let index: number
            index = 0;
            for (let x= 0; x < this.subcategories.length; x++) {
              
              
                      if (this.subcategories[x].parentCategory === this.selectedCategoriesItems[0].itemName) {
              
                        this.dropdownSubCategoriesObject = new DropDown(index, this.subcategories[x].name)
                        this.dropdownSubCategoriesList.push(this.dropdownSubCategoriesObject)
              
                        index++
                      }
                    
              
                    }
            for (let j = 0; j < this.dropdownSubCategoriesList.length; j++) {
              
                          if (this.item.subCategory === this.dropdownSubCategoriesList[j].itemName) {
                            
                            // this.dropcategory=true;
                             this.selectedSubCategoriesItems[0] = new DropDown(j, this.dropdownSubCategoriesList[j].itemName);
                         
                          }
                        
                    }
            
          });
     

    // this.selectedCategoriesItems[0] = new DropDown(0, this.item.category);
    // this.selectedCusinesItems[0] = new DropDown(0, this.item.cuisine);
    // this.selectedSubCategoriesItems[0] = new DropDown(0, this.item.subCategory);;

    this.dropdownCategoriesSettings = {
      singleSelection: true,
      text: "Select Category",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: false,
      classes: "myclass custom-class"
    };
    this.dropdownCusinesSettings = {
      singleSelection: true,
      text: "Select Cusine",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: false,
      classes: "myclass custom-class"
    };
    this.dropdownSubCategoriesSettings = {
      singleSelection: true,
      text: "Select Sub Category",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: false,
      classes: "myclass custom-class"
    };

  }

  onItemSelect(item: any) {
    console.log(item);
    if (this.selectedCategoriesItems.length > 0) {

      this.dropdownSubCategoriesList = [];
      let index: number
      index = 0;
      for (let j = 0; j < this.subcategories.length; j++) {


        if (this.subcategories[j].parentCategory === this.selectedCategoriesItems[0].itemName) {

          this.dropdownSubCategoriesObject = new DropDown(index, this.subcategories[j].name)
          this.dropdownSubCategoriesList.push(this.dropdownSubCategoriesObject)

          index++
        }
        if (item.itemName === this.selectedCategoriesItems[0].itemName) {


          this.selectedSubCategoriesItems = []

        }

      }



    }

  }
  OnItemDeSelect(item: any) {
    console.log(item);

    if (this.selectedCategoriesItems.length < 1) {

      this.dropdownSubCategoriesList = [];
      this.selectedSubCategoriesItems = [];
    }
  }
  showList(value: string) {


    if (value === "yes") {
      // this.getingredientLists();
      this.showListOfIngredients = true;


      // if (this.ingredientlists.length < 1) {

      //   this.ingredientlist = new namesoflist();
      //   this.ingredientlist.name = "";
      //   this.ingredientlists.push(this.ingredientlist);
      // }



    } else {
      this.showListOfIngredients = false;
      this.ingredientlists.splice(0, this.ingredientlists.length);
      // this.ingredientlists.splice(0, this.ingredientlists.length)
       this.item.ingredientListNames.splice(0, this.item.ingredientListNames.length);
      this.item.ingredientListPrices.splice(0, this.item.ingredientListPrices.length);
      // this.listnames.splice(0, this.listnames.length)


    }

  }
  pluslsit(i, ingredientlists) {

    this.ingredientlist = new namesoflist();
    this.ingredientlist.name = "";
    this.ingredientlists.push(this.ingredientlist);

    for (let i = 0; i < this.ingredientlists.length; i++) {
      this.item.ingredientListNames[i] = ingredientlists[i].name;
      this.item.ingredientListPrices[i] = ingredientlists[i].price;
    }

    console.log(this.ingredientlists)

  }
  minuslist(i, ingredientlists) {

    this.ingredientlists.splice(i, 1);

    this.item.ingredientListNames.splice(i, 1);
    this.item.ingredientListPrices.splice(i, 1);

    console.log(this.ingredientlists)


    if (this.ingredientlists.length < 1) {
      this.radioyes.nativeElement.checked = false
      this.radiono.nativeElement.checked = true;
    }


  }
  getSelectedingredientlist(selectedingredientlist) {
    this.item.listIngredienttype=selectedingredientlist.type;
    this.ingredientlists.splice(0, this.ingredientlists.length)
    this.item.ingredientListNames.splice(0, this.item.ingredientListNames.length);
    this.item.ingredientListPrices.splice(0, this.item.ingredientListPrices.length);
    this.listsplit = selectedingredientlist.name.split("/");

    for (let l = 0; l < this.listsplit.length; l++) {
      this.ingredientlist = new namesoflist();
      this.ingredientlist.name = this.listsplit[l];
      this.ingredientlist.price = "0";
      this.ingredientlists.push(this.ingredientlist);
    }
    // console.log(selectedingredientlist)
    this.showlist = true;


  }

  //  getingredientLists() {
  //   this.ingredientListService.getIngredientListList().subscribe(res => {
  //     this.ingredientLists = res
  //     console.log(this.ingredientLists)
  //     // this.selectedingredientlist = ""
  //     let index:number=0;
  //     for (let i = 0; i < this.ingredientLists.length; i++) {
  //       if (this.ingredientLists[i].IngredientListStatus === "Active"){
  //         this.listname= new listnameAndtype();
  //         this.listname.name = this.ingredientLists[i].names.join("/");
  //         this.listname.type=this.ingredientLists[i].IngredientType
  //         this.listnames.push(this.listname)
  //         index++;
  //       }
  //     }
  //   });
  // }
  update(item: Item, ingredientlists) {

    // console.log(item);
    //  console.log(ingredientlists);
    item.category = this.selectedCategoriesItems[0].itemName;
    item.cuisine = this.selectedCusinesItems[0].itemName;
    item.subCategory = this.selectedSubCategoriesItems[0].itemName;
    for (let i = 0; i < ingredientlists.length; i++) {

      item.ingredientListNames[i] = ingredientlists[i].name;
      item.ingredientListPrices[i] = ingredientlists[i].price;
    }
    //  console.log(item);
    this.itemService.updateItem(item._id, item).subscribe(res => {
      console.log(res)
      for (let i = 0; i < this.updateItems.length; i++) {
        if (this.updateItems[i]._id === item._id) {
          this.updateItems[i].itemName=item.itemName
          this.updateItems[i].category=item.category
          if(this.admin===true){
            this.updateItems[i].approval=item.approval
          }
       
          this.updateItems[i]. cuisine=item.cuisine
          this.updateItems[i]. itemStatus=item.itemStatus;

        }
      }
    });

    this.updatedItems.emit(this.updateItems); 



  }
  back(): void {

    this.isBack.emit(false);

  }

}
