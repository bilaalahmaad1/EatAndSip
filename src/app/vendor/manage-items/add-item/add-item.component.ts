import { Component, OnInit, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { Category, categoryService } from '../../../admin/manage-category/services/category.services';
import { subCategory, subcategoryService } from '../../../admin/manage-category/services/subCategory.service';
import { Cuisine, cuisineService } from '../../../admin/manage-cuisines/services/cuisine.service';
import { Item, itemService } from '../services/item.service';
import { IngredientList, ingredientListService } from '../../../admin/manage-ingredient/services/ingredientlist.service';
import { IngredientType, ingredienttypeService } from '../../../admin/manage-ingredient/services/ingredienttype.service';
import { profileService } from '../../../profile/service/profile.service';
import{VendorDetail,vendorDetailService}from '../../../admin/manage-vendor/services/vendorDetail.service'

class DropDown {

  constructor(public id: number,
    public itemName: string) { }
}
class listnameAndtype {

  public name: string = ""
  public type: string = ""

}
class namesoflist {
  public name: string = "";
  public price: string = "";
}
@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent implements OnInit {
  Vendor: string;
  vendors: VendorDetail[];
  isApproval: string;
  admin: boolean;
  vendorName: string;
  type: string = ""
  selectedingredientlist: string = "";
  showlist: boolean = false;
  listsplit: string[];
  ingredientlists: namesoflist[] = [];
  ingredientlist: namesoflist;
  listnames: listnameAndtype[] = [];
  listname: listnameAndtype;
  ingredientLists: IngredientList[] = [];
  showListOfIngredients: boolean;
  isStatus: any;
  @Output() isBack: EventEmitter<boolean> = new EventEmitter<boolean>();
  ingredientTypes: IngredientType[] = [];
  newItem: Item;
  categories: Category[] = [];
  tempcategories: any;
  selectedCategoriesItems = [];
  dropdownCategoriesSettings = {};
  dropdownCategoriesObject: DropDown;
  dropdownCategoriesList: DropDown[] = []

  subcategories: subCategory[] = [];
  tempsubcategories: any;
  selectedSubCategoriesItems = [];
  dropdownSubCategoriesSettings = {};
  dropdownSubCategoriesObject: DropDown;
  dropdownSubCategoriesList: DropDown[] = []

  tempcusines: any;
  cusines: Cuisine[] = [];
  selectedCusinesItems = [];
  dropdownCusinesSettings = {};
  dropdownCusinesObject: DropDown;
  dropdownCusinesList: DropDown[] = []
  @ViewChild('myRadioyes') public radioyes: ElementRef;
  @ViewChild('myRadiono') public radiono: ElementRef;
  @Output() item: EventEmitter<Item> = new EventEmitter<Item>();

  constructor(public categoryService: categoryService,
    public cuisineService: cuisineService,
    public subcategoryService: subcategoryService,
    public itemService: itemService,
    public ingredienttypeService: ingredienttypeService,
    public ingredientListService: ingredientListService,
    public profileService: profileService,
    public vendorDetailService:vendorDetailService

  ) {
    this.newItem = new Item();
    this.ingredientlist = new namesoflist();
    this.newItem.ingredientListNames = [];
    this.newItem.ingredientListPrices = [];
    this.listsplit = [];
    // this.listnames[0].name
  }

  ngOnInit() {
    // this.profileService.getUser().subscribe(res => {
     
    // });
    this.vendorDetailService.getVendorDetailList().subscribe(res=>{this.vendors=res});

    this.ingredienttypeService.getIngredientTypeList().subscribe(res => { this.ingredientTypes = res });
    this.profileService.getUser().subscribe(res => {
      if (res.user.vendor) {
        this.vendorName = res.user.vendor.vendorName;
      }
      if (res.user.admin) {
        this.admin = true
      }
    });
    this.categoryService.getCategoryList()
      .subscribe(res => {
        this.tempcategories = res;
        for (let i = 0; i < this.tempcategories.length; i++) {
          if (this.tempcategories[i].categoryStatus === "Active") {
            this.categories.push(this.tempcategories[i]);
          }
        }
        for (let i = 0; i < this.categories.length; i++) {

          this.dropdownCategoriesObject = new DropDown(i, this.categories[i].name)
          this.dropdownCategoriesList.push(this.dropdownCategoriesObject)
        }
      });
    this.cuisineService.getCuisineList()
      .subscribe(res => {
        // console.log("i am in cusine service");
        this.tempcusines = res;
        for (let i = 0; i < this.tempcusines.length; i++) {
          if (this.tempcusines[i].cusineStatus === "Active") {
            this.cusines.push(this.tempcusines[i]);
          }
        }


        console.log(this.cusines);
        for (let i = 0; i < this.cusines.length; i++) {
          // console.log("i am in cusine service lop");
          this.dropdownCusinesObject = new DropDown(i, this.cusines[i].name)
          this.dropdownCusinesList.push(this.dropdownCusinesObject)
        }

        this.subcategoryService.getsubCategoryList()
          .subscribe(res => {
            console.log("sub category");
            console.log(res);

            this.tempsubcategories = res;
            for (let i = 0; i < this.tempsubcategories.length; i++) {
              if (this.tempsubcategories[i].sucCategoryStatus === "Active") {
                this.subcategories.push(this.tempsubcategories[i]);
              }
            }
          });
      });
    this.selectedCategoriesItems = [];
    this.selectedCusinesItems = [];
    this.selectedSubCategoriesItems = [];

    this.dropdownCategoriesSettings = {
      singleSelection: true,
      text: "Select Category",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: false,
      classes: "myclass custom-class"
    };
    this.dropdownCusinesSettings = {
      singleSelection: true,
      text: "Select Cusine",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: false,
      classes: "myclass custom-class"
    };
    this.dropdownSubCategoriesSettings = {
      singleSelection: true,
      text: "Select Sub Category",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: false,
      classes: "myclass custom-class"
    };


  }
  onItemSelect(item: any) {
    console.log(item);
    if (this.selectedCategoriesItems.length > 0) {

      this.dropdownSubCategoriesList = [];
      let index: number
      index = 0;
      for (let j = 0; j < this.subcategories.length; j++) {


        if (this.subcategories[j].parentCategory === this.selectedCategoriesItems[0].itemName) {

          this.dropdownSubCategoriesObject = new DropDown(index, this.subcategories[j].name)
          this.dropdownSubCategoriesList.push(this.dropdownSubCategoriesObject)

          index++
        }
        if (item.itemName === this.selectedCategoriesItems[0].itemName) {


          this.selectedSubCategoriesItems = []

        }

      }



    }

  }
  OnItemDeSelect(item: any) {
    console.log(item);

    if (this.selectedCategoriesItems.length < 1) {

      this.dropdownSubCategoriesList = [];
      this.selectedSubCategoriesItems = [];
    }



  }
  getSelectedingredientlist(selectedingredientlist,value) {
    this.newItem.listIngredienttype = selectedingredientlist.type;
    this.newItem.listIngredientName=value;
    this.ingredientlists.splice(0, this.ingredientLists.length)
    this.listsplit = selectedingredientlist.name.split("/");

    for (let l = 0; l < this.listsplit.length; l++) {
      this.ingredientlist = new namesoflist();
      this.ingredientlist.name = this.listsplit[l];
      this.ingredientlist.price = "0";
      this.ingredientlists.push(this.ingredientlist);
    }
    console.log(selectedingredientlist)
    this.showlist = true;


  }
  save(ingredientlists) {
    this.newItem.category = this.selectedCategoriesItems[0].itemName;
    this.newItem.subCategory = this.selectedSubCategoriesItems[0].itemName;
    this.newItem.cuisine = this.selectedCusinesItems[0].itemName;
    this.newItem.itemStatus = this.isStatus;
    if(this.admin===true){
      this.newItem.vendorName=this.Vendor;
      this.newItem.approval = this.isApproval
    }else{
      this.newItem.vendorName = this.vendorName;
      this.newItem.approval = "No";
    }
    
   
   
    for (let i = 0; i < ingredientlists.length; i++) {
      this.newItem.ingredientListNames[i] = ingredientlists[i].name;
      this.newItem.ingredientListPrices[i] = ingredientlists[i].price;
    }
    console.log(this.newItem)
    this.item.emit(this.newItem);
  }

  back(): void {

    this.isBack.emit(true);

  }
  status(isStatus) {
    this.isStatus = isStatus;
  }
  approval(isApproval) {

    this.isApproval = isApproval
  }
  showList(value: string) {
    if (value === "yes") {
      this.getingredientLists();
      this.showListOfIngredients = true;
      this.newItem.listIngredientName="";
    }
    else {
      this.showListOfIngredients = false;
      this.ingredientlists.splice(0, this.ingredientlists.length)
      this.listnames.splice(0, this.listnames.length)
    }

  }
  getingredientLists() {
    this.ingredientListService.getIngredientListList().subscribe(res => {
      this.ingredientLists = res
      console.log(this.ingredientLists)
      this.selectedingredientlist = ""
      let index: number = 0;
      for (let i = 0; i < this.ingredientLists.length; i++) {
        if (this.ingredientLists[i].IngredientListStatus === "Active") {
          this.listname = new listnameAndtype();
          this.listname.name = this.ingredientLists[i].names.join("/");
          this.listname.type = this.ingredientLists[i].IngredientType
          this.listnames.push(this.listname)
          index++;
        }



      }
    });
  }

  pluslsit(i, ingredientlists) {

    this.ingredientlist = new namesoflist();
    this.ingredientlist.name = "";
    this.ingredientlist.price = "0";
    this.ingredientlists.push(this.ingredientlist);
    console.log(this.ingredientlists)
    for (let i = 0; i < ingredientlists.length; i++) {
      this.newItem.ingredientListNames[i] = ingredientlists[i].name;
      // this.newIngredientList.prices[i]=ingredientlists[i].price;
    }
    console.log(this.newItem)



  }
  minuslist(i, ingredientlists) {

    this.ingredientlists.splice(i, 1);

    // this.newIngredientList.names.splice(i, 1);
    // this.newIngredientList.prices.splice(i, 1);

    console.log(this.ingredientlists)


    if (this.ingredientlists.length < 1) {
      this.showListOfIngredients = false;
      this.radiono.nativeElement.checked = true
    }


  }
  onSelected() {
    console.log("haha")
  }
  getvendor(vendor){
    console.log(vendor)
    this.Vendor=vendor
  }
}
