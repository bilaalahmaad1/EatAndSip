import { Component, OnInit } from '@angular/core';
import {Item,itemService} from './services/item.service';
import { Router } from '@angular/router';
import {profileService} from'../../profile/service/profile.service';

@Component({
  selector: 'app-manage-items',
  templateUrl: './manage-items.component.html',
  styleUrls: ['./manage-items.component.css']
})
export class ManageItemsComponent implements OnInit {
  vendor: boolean;
  admin: boolean;
  vendorName: string;

  Items: Item[] = [];
  resItems: Item;
  id: number;
  
  mesg: any;
  isCreate: Boolean
  isAdd: Boolean
  isUpdate: Boolean
  constructor(public itemService:itemService,public router:Router , public profileService:profileService) { 
    this.isAdd = false;
    this.isCreate = true;
    this.isUpdate = false
  }
  
  ngOnInit() {
    this.profileService.getUser().subscribe(res => {
      
      if (res.user.vendor) {
       this.vendor=true
       this.vendorName= res.user.vendor.vendorName;
       this.getItems();
      }else{
        this.admin=true
        this.itemService.getItemList().subscribe(res=>{  console.log(res); this.Items=res});
      }
    });
    
   
  }
  getItems(){
    this.itemService.getItemList().subscribe(res=>{
      console.log(res)
      if (res.success === false) { this.router.navigate(['/login']);}
       let index:number=0
     for(let i=0;i<res.length;i++){
      if( res[i].vendorName===this.vendorName){
      
        this.Items[index]=res[i];
        index++
      }
     }
     
     });
  }
  createItem() {
    this.isAdd = true;
    this.isCreate = false;
  }
  update(id) {
    this.isCreate = false;
    this.isUpdate = true;
     this.id = id
  }
  showItemPage(condition: boolean) {
    // this.getVendorDetails();
    if (condition === true) {
      this.isAdd = false;
      this.isCreate = true;
    }
    else {
      this.isCreate = true;
      this.isUpdate = false;
    }

  }
  addItem(item) {
    
        this.isAdd = false;
        this.isCreate = true;
    
        this.itemService.postItem(item)
          .subscribe(responce => {
            this.resItems = responce
            this.Items.push(this.resItems);
          });
    
      }
      delete(id: Number): void {
        // console.log(id);
        // this.getCountries();
        this.itemService.deleteItem(id)
          .subscribe(responce => {
            this.mesg = responce
            //  console.log(this.mesg);
            for (let i = 0; i < this.Items.length; i++) {
              if (id === this.Items[i]._id) {
                let index;
                index = i;
                
               
               
    
    
                this.Items.splice(index, 1)
                this.Items = this.Items;
    
              }
            }
          });
    
    
      }
      updateItem(updatedItems){
        this.Items=updatedItems;
        // console.log("i am in updateCountry function")
        // console.log(this.vendorDetails)
          this.isCreate=true;
        this.isUpdate=false;
      }

}
