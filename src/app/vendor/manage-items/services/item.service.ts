import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { HttpClient } from '../../../shared/common/httpClient';
import { Headers, RequestOptions } from '@angular/http';
export class Item {
    public _id: number;

    public category:string
    public cuisine:string
    public subCategory:string
    public itemName:string;
    public itemPricePerUnit:string;
    public itemStatus:string;
    public ingredientListNames:string[];
    public ingredientListPrices:string[];
    public listIngredienttype:string
   public vendorName:string;
   public approval:string;
   public  listIngredientName:string;
    
}
@Injectable()
export class itemService {
    
        constructor(private http: Http, private HttpClient: HttpClient) {
        }
    
        getItem(id: number) {
            return this.getItemList()
                .map(Items => Items.find(Item => Item.id === id))
                // .do(value => (console.log(value)))
                .catch(this.handlError);
        }
    
        getItemList() {
    
            // return this.http
            //     .get('assets/city.json')
            //     .map((response: Response) => <City[]>response.json().City)
            //     // .do(value => console.log(value))
            //     .catch(this.handlError);
            let url = "items";
            // console.log("sbvhk")
            return this.HttpClient
                .get(url)
                .map((response: Response) => {
                    let data = response.json();
                    return data;
                });
        }
        postItem(item: Item) {
            let url = "items"
            let headers = new Headers({ 'Content-Type': 'application/json' });
            let options = new RequestOptions({ headers: headers });
            return this.HttpClient
                .post(url, item)
                .map((response: Response) => {
                    let data = response.json();
                    return data;
                });
        }
        updateItem(id, item: Item) {
            let url = "items/" + id
            let headers = new Headers({ 'Content-Type': 'application/json' });
            let options = new RequestOptions({ headers: headers });
            return this.HttpClient
                .put(url, item)
                .map((response: Response) => {
                    let data = response.json();
                    return data;
                });
        }
        deleteItem(id) {
            console.log(id)
            let url = "items/" + id
            let headers = new Headers({ 'Content-Type': 'application/json' });
            let options = new RequestOptions({ headers: headers });
            return this.HttpClient
                .delete(url)
                .map((response: Response) => {
                    let data = response.json();
                    return data;
                });
        }
    
        private handlError(error: Response) {
            let msg = `Error status code${error.status} at ${error.url}`;
            return Observable.throw(msg);
        }
    }