import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageItemsComponent } from './manage-items.component';
import { AddItemComponent } from './add-item/add-item.component';
import { UpdateItemComponent } from './update-item/update-item.component';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { AngularMultiSelectModule } from '../../../../node_modules/angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { itemService } from './services/item.service';
@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    BrowserModule,
    FormsModule,

    AngularMultiSelectModule

  ],
  providers: [itemService],
  declarations: [ManageItemsComponent, AddItemComponent, UpdateItemComponent],
  exports: [ManageItemsComponent]
})
export class ManageItemsModule { }
