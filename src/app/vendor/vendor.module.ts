import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorComponent } from './vendor.component';
import { NavigationComponent } from './navigation/navigation.component';
import { RouterModule } from '@angular/router';
import { ManageIngredientModule } from '../admin/manage-ingredient/manage-ingredient.module';
import { ManageItemsModule } from './manage-items/manage-items.module';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import {ManageOrderModule} from './manage-order/manage-order.module';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ManageIngredientModule,
    ManageItemsModule,
    ManageOrderModule,
    HttpModule,
    BrowserModule,
    FormsModule

  ],
  declarations: [VendorComponent, NavigationComponent]
})
export class VendorModule { }
