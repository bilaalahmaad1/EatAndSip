import { Component, OnInit, Injectable } from '@angular/core';
import { LoginModel } from './model/login.model';
import { loginService } from './service/login.service';
import { Router } from '@angular/router';
import { adminsService } from '../admin/service/admins.service';
import { Role } from '../Role/role';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login: LoginModel;
  admins: any;
  // Role: Role;

  constructor(public loginService: loginService, private router: Router, public adminsService: adminsService) {
    this.login = new LoginModel();
    // this.Role = new Role();


  }

  ngOnInit() {
    localStorage.removeItem("login");





  }
  signin(): void {

    this.loginService.getToken(this.login).subscribe(Response => {

      // console.log(Response);
      let data = Response


      if (data.success === true && data.admin) {
        // this.Role.userNanme = data.admin.userName;
        // this.Role.address = data.admin.address
        // this.Role.email = data.admin.email
        // this.Role.phone = data.admin.phone
        // this.Role.type = data.admin.type




        this.router.navigate(['/admin']);

        localStorage["login"] = JSON.stringify(data.token);




      }
      else if (data.success === true && data.vendor) {
     
        if (data.vendor.vendorStatus === "Active") {
          this.router.navigate(['/vendor']);

          localStorage["login"] = JSON.stringify(data.token);
        }else{
          console.log("you are "+data.vendor.vendorStatus+"by admin");

        }


      } else {
        console.log("sign up first")
      }


    });
  }

}
