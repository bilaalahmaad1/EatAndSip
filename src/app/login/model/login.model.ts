export class LoginModel {
    public email: string;
    public password: string;
    public userName: string;
    public address: string;
    public phone: string;
    public type: string;
}