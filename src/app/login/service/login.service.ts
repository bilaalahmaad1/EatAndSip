import { Injectable } from '@angular/core';
import {  Response } from '@angular/http';
// import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { LoginModel } from '../model/login.model';
// import { ErrorObservable } from "rxjs/observable/ErrorObservable";
// import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '../../shared/common/httpClient';

@Injectable()
export class loginService {
    constructor( public httpclient: HttpClient) { }

    getToken(login: LoginModel) {
        let url = "login";
        return this.httpclient
            .post(url, login)
            .map((response: Response) => {
                console.log("membership service")
                let data = response.json();
                return data;
            });
    }
}
