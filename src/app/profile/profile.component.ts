import { Component, OnInit, Injectable } from '@angular/core';
import { profileService } from './service/profile.service';
import { VendorDetail, vendorDetailService } from '../admin/manage-vendor/services/vendorDetail.service';
import { Router } from '@angular/router';




@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  userName: string = "";
  address: string = "";
  email: string = "";
  phone: string = "";
  type: string = "";
  isBack: boolean = false;
  readonly: boolean = true;
  isUpdate: boolean = true;
  updateNow: boolean = false;
  admin: boolean = false;
  vendor: boolean = false;
  id: number;
  vendorDetail: VendorDetail
  constructor(public profileService: profileService, public vendorDetailService: vendorDetailService,public router:Router) {
    this.vendorDetail = new VendorDetail();

  }

  ngOnInit() {
    this.profileService.getUser().subscribe(res => {

      if (res.user.vendor) {
        this.admin = false
        this.vendor = true;
        // console.log(res.user.vendor);
        this.id = res.user.vendor._id
        this.userName = res.user.vendor.userName
        this.address = res.user.vendor.contactAddress
        this.email = res.user.vendor.email
        this.phone = res.user.vendor.vendorSMSnumber
        this.type=res.user.vendor.type
        this.vendorDetail=res.user.vendor;
        
      }

      if (res.user.admin) {
        this.admin = true
        this.vendor = false;
        this.id = res.user.admin._id
        this.userName = res.user.admin.userName
        this.address = res.user.admin.address
        this.email = res.user.admin.email
        this.phone = res.user.admin.phone
        this.type = res.user.admin.type

        // console.log(res.user.admin);
      }


    });






  }
  back() {
    this.readonly = true;
    this.isBack = false
    this.isUpdate = true;
    this.updateNow = false
  }
  update() {
    this.readonly = false;
    this.isBack = true
    this.isUpdate = false;
    this.updateNow = true
  }
  updatenow() {
    if (this.admin === true) {
      console.log("i am updating admin");
    }
    if (this.vendor === true) {
      console.log("i am updating vendor");
      this.vendorDetail.userName=this.userName;
       this.vendorDetail.contactAddress=this.address 
      this.vendorDetail.email=this.email
      this.vendorDetail.vendorSMSnumber=this.phone
      console.log( this.vendorDetail)
       this.vendorDetailService.updateVendorDetail(this.vendorDetail._id,this.vendorDetail).subscribe(res=>{ this.router.navigate(['/login']);}
      );
    }
  }

}
