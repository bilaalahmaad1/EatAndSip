import { Injectable } from '@angular/core';
import {  Response } from '@angular/http';
 import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { HttpClient } from '../../shared/common/httpClient'

@Injectable()
export class profileService {
    constructor( public httpclient: HttpClient) { }

    getUser() {
        let url = "user";
        return this.httpclient
            .get(url)
            .map((response: Response) => {
                //  console.log("membership service")
                let data = response.json();
                return data;
            });
    }
    private handlError(error: Response) {
        let msg = `Error status code${error.status} at ${error.url}`;
        return Observable.throw(msg);
    }
}
