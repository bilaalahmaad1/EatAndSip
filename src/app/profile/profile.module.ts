import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';
import { profileService } from './service/profile.service';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    RouterModule,
    BrowserModule,
    
    FormsModule
  ],
  declarations: [ProfileComponent],
  providers: [ profileService],
  exports: [ProfileComponent],
  

})
export class ProfileModule { }
