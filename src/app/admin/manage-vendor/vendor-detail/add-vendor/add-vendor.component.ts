import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { VendorDetail } from '../../services/vendorDetail.service';

import { Category, categoryService } from '../../../manage-category/services/category.services';
import { Cuisine, cuisineService } from '../../../manage-cuisines/services/cuisine.service';
import { City, cityService } from '../../../manage-address/services/city.service';
import { Area, areaService } from '../../../manage-address/services/area.service';


class DropDown {

  constructor(public id: number,
    public itemName: string) { }
}
@Component({
  selector: 'app-add-vendor',
  templateUrl: './add-vendor.component.html',
  styleUrls: ['./add-vendor.component.css']
})

export class AddVendorComponent implements OnInit {
  
  tempareas: any;
  tempcities: any;
  tempcusines: any;
  tempcategories: any;
  categories: Category[]=[];
  cusines: Cuisine[]=[];
  cities: City[]=[];
  areas: Area[]=[];
  temparea: string;
  tempcity: string;
  temcategories: string[] = [];
  temcusines: string[] = [];
  templocations: string[]=[];
  // dropdownList=[];
  selectedCategoriesItems = [];
  dropdownCategoriesSettings = {};
  dropdownCategoriesObject: DropDown;
  dropdownCategoriesList: DropDown[] = []
  
  selectedCusinesItems = [];
  dropdownCusinesSettings = {};
  dropdownCusinesObject: DropDown;
  dropdownCusinesList: DropDown[] = []

  selectedCityItems = [];
  dropdownCitySettings = {};
  dropdownCityObject: DropDown;
  dropdownCityList: DropDown[] = []
  selectedAreaItems = [];
  dropdownAreaSettings = {};
  dropdownAreaObject: DropDown;
  dropdownAreaList: DropDown[] = []

  selectedlocationsItems = [];
  dropdownlocationSettings = {};
  dropdownlocationObject: DropDown;
  dropdownlocationList: DropDown[] = []
  @Output () isBack: EventEmitter<boolean> = new EventEmitter<boolean>();
  isStatus:any;

  @Output() vendorDetail: EventEmitter<VendorDetail> = new EventEmitter<VendorDetail>();
  newVendorDetail: VendorDetail;

  constructor(public areaServce: areaService, public categoryService: categoryService, public cuisineService: cuisineService, public cityService: cityService, public areaService: areaService) {
    this.newVendorDetail = new VendorDetail();
    this.isStatus="";
  }


  ngOnInit() {

    this.categoryService.getCategoryList()
      .subscribe(res => {
        this.tempcategories=res; 
        for(let i =0;i<this.tempcategories.length;i++){
          if(this.tempcategories[i].categoryStatus==="Active"){
            this.categories.push(this.tempcategories[i]);
          }
        }
       
        
       
        for (let i = 0; i < this.categories.length; i++) {
          // console.log("i am in categoryservice loop");
          this.dropdownCategoriesObject = new DropDown(i, this.categories[i].name)
          this.dropdownCategoriesList.push(this.dropdownCategoriesObject)
        }

      });
    this.cuisineService.getCuisineList()
      .subscribe(res => {
        // console.log("i am in cusine service");
        this.tempcusines=res; 
        for(let i =0;i<this.tempcusines.length;i++){
          if(this.tempcusines[i].cusineStatus==="Active"){
            this.cusines.push(this.tempcusines[i]);
          }
        }
        

        console.log(this.cusines);
        for (let i = 0; i < this.cusines.length; i++) {
          // console.log("i am in cusine service lop");
          this.dropdownCusinesObject = new DropDown(i, this.cusines[i].name)
          this.dropdownCusinesList.push(this.dropdownCusinesObject)
        }

      });

    this.cityService.getCityList()
      .subscribe(response => {
        this.tempcities=response; 
        for(let i =0;i<this.tempcities.length;i++){
          if(this.tempcities[i].cityStatus==="Active"){
            this.cities.push(this.tempcities[i]);
          }
        }
       
        // console.log("i am in city service");
        console.log(this.cities)
        for (let j = 0; j < this.cities.length; j++) {
          // console.log("i am in city service loop");
          this.dropdownCityObject = new DropDown(j, this.cities[j].name)
          this.dropdownCityList.push(this.dropdownCityObject)
        }
      });


    this.areaServce.getAreaList()
      .subscribe(response => {
        this.tempareas=response; 
        for(let i =0;i<this.tempareas.length;i++){
          if(this.tempareas[i].areaStatus==="Active"){
            this.areas.push(this.tempareas[i]);
          }
        }
     
      });


    this.selectedCategoriesItems = [];
    this.selectedCusinesItems = [];
    this.selectedCityItems = [];


    this.dropdownCategoriesSettings = {
      singleSelection: false,
      text: "Select Categories",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };
    this.dropdownCusinesSettings = {
      singleSelection: false,
      text: "Select Cusines",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };
    this.dropdownCitySettings = {
      singleSelection: true,
      text: "Select City",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };
    this.dropdownAreaSettings = {
      singleSelection: true,
      text: "Select Area",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };
    this.dropdownlocationSettings = {
      singleSelection: false,
      text: "Select Location",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };

  }
  onItemSelect(item: any) {
    console.log(item);
    console.log("selectedCategoriesItems");
    console.log(this.selectedCategoriesItems);

    if (this.selectedCityItems.length > 0) {
      // console.log("city added");


      // console.log("i am in area service");
      console.log(this.areas)
      this.dropdownAreaList = [];
      this.dropdownlocationList=[];
      // this.selectedAreaItems=[]
      for (let j = 0; j < this.areas.length; j++) {
        // console.log("i am in area service loop");

        if (this.areas[j].cityName === this.selectedCityItems[0].itemName) {
          // this.selectedAreaItems=[]
          this.dropdownAreaObject = new DropDown(j, this.areas[j].name)
          this.dropdownAreaList.push(this.dropdownAreaObject)
          this.dropdownlocationList.push(this.dropdownAreaObject)
          // this.dropdownlocationObject = new DropDown(j, this.areas[j].name)
        
        }
        if (item.itemName === this.selectedCityItems[0].itemName) {
          // console.log("i am in deleting");

          this.selectedAreaItems = []
          this.selectedlocationsItems=[]
          // this.dropdownlocationList=[]
        }

      }



    }
  }
  OnItemDeSelect(item: any) {
    console.log(item);


    if (this.selectedCityItems.length < 1) {
      this.dropdownAreaList = [];
      this.selectedAreaItems = [];
      this.dropdownlocationList=[];
      this.selectedlocationsItems =[];
    }

    // console.log(this.selectedItems2);
  }
  onSelectAll(items: any) {

    console.log(items);
  }
  onDeSelectAll(items: any) {

    console.log(items);


  }
  save(): void {
    // back to country with country
    // console.log(this.newVendorDetail);
    // console.log( this.selectedCategoriesItems);
    for (let i = 0; i < this.selectedCategoriesItems.length; i++) {

      this.temcategories[i] = this.selectedCategoriesItems[i].itemName
    }
    for (let i = 0; i < this.selectedCusinesItems.length; i++) {
      
            this.temcusines[i] = this.selectedCusinesItems[i].itemName
          }
          for (let i = 0; i < this.selectedlocationsItems.length; i++) {
            
                  this.templocations[i] = this.selectedlocationsItems[i].itemName
                }

    this.newVendorDetail.categories = this.temcategories
    this.newVendorDetail.cusines = this.temcusines;
    this.newVendorDetail.deliveryLocation=this.templocations;
    this.newVendorDetail.city = this.selectedCityItems[0].itemName;
    this.newVendorDetail.area = this.selectedAreaItems[0].itemName;
    this.newVendorDetail.vendorStatus=this.isStatus;
    this.newVendorDetail.type="vendor"
    console.log(this.newVendorDetail)
     this.vendorDetail.emit(this.newVendorDetail);
  }
  back(): void {

this.isBack.emit(true);

  }
  status(isStatus){
    this.isStatus=isStatus;
  }


}
