import { Component, OnInit ,Input } from '@angular/core';
import {VendorDetail,vendorDetailService} from '../services/vendorDetail.service'; 
import { Router } from '@angular/router';

@Component({
  selector: 'app-vendor-detail',
  templateUrl: './vendor-detail.component.html',
  styleUrls: ['./vendor-detail.component.css']
})
export class VendorDetailComponent implements OnInit {
  vendorDetails: VendorDetail[];
  isCreate: boolean;
  isAdd: boolean;
  isUpdate: boolean;
  id:number;
  resVendorDetail:VendorDetail;
  mesg:string;
  constructor(public vendorDetailService:vendorDetailService,public router:Router) {
    this.vendorDetails = [];
    this.isCreate = true;
    this.isAdd = false;
    this.isUpdate = false;
    this.mesg="";
   }

   ngOnInit() {
    this.getVendorDetails();
  }
  getVendorDetails(): void {
    this.vendorDetailService.getVendorDetailList()
      .subscribe(responce => {
        if (responce.success === false) { this.router.navigate(['/login']);}
        this.vendorDetails = responce
        console.log("i am in manage contry get vendorDetails from service");
        console.log(this.vendorDetails);
        // console.log("jjjjjjjjjjjjjj")
        // console.log(this.countries)
      });
  }
  createVendorDetail(): void {
    console.log("create button hits");
    this.isAdd = true;
    this.isCreate = false;
    // this.router.navigate(['/Country/Add']); 
  }
  addVendorDetail(vendorDetail: VendorDetail) {
    // console.log("I am in addCountry function new country comes from add component");
    console.log(vendorDetail);
    this.isAdd = false;
    this.isCreate = true;
    
    //add post method of service to send data in data base 
    // console.log("we are adding countru to servise to db");
    this.vendorDetailService.postVendorDetail(vendorDetail)
    .subscribe(responce => {
        this.resVendorDetail = responce
      //  console.log("log1")
      // console.log("after adding new country to db we get this response");
        console.log(this.resVendorDetail)
        //  console.log("log2")
    this.vendorDetails.push(vendorDetail);
    // console.log(this.countries);
      });
      


  }
  delete(id: Number): void {
    // use load ash here 
    //  let index=_.findIndex(this.countries, function(o) { return n == id; });
   // console.log(this.countries)
    console.log(id);
    this.vendorDetailService.deleteVendorDetail(id)
    .subscribe(responce => {
        this.mesg = responce
        console.log(this.mesg);
         for (let i = 0; i < this.vendorDetails.length; i++) {
      if (id === this.vendorDetails[i]._id) {
        let index;
        index = i;
       console.log(this.vendorDetails[i]._id)
        // console.log(index)
        this.vendorDetails.splice(index, 1)
        // console.log(this.countries)
        this.vendorDetails = this.vendorDetails;
        //  this.newemployeedata=this.delEmployee
        // console.log(this.countries)
        // console.log(id)
      }
    }
      
    
    
      });
   
  }
  update(id):void{
    console.log("HIT UPDATE BUTTON");
    this.isCreate=false;
    this.isUpdate=true;
    this.id=id
    //  this.router.navigate(['Country/Update',id]); 
  }
  updateVendorDetail(updatedVendorDetails:VendorDetail[]):void{
    this.vendorDetails=updatedVendorDetails;
    // console.log("i am in updateCountry function")
    // console.log(this.vendorDetails)
      this.isCreate=true;
    this.isUpdate=false;
  }
  
  showVendorPage(condition: boolean) {
    this.getVendorDetails();
    if (condition === true) {
      this.isAdd = false;
      this.isCreate = true;
    }
    else {
      this.isCreate = true;
      this.isUpdate = false;
    }

  }
}

