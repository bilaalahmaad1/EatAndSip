import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { VendorDetail, vendorDetailService } from '../../services/vendorDetail.service';
import { Category, categoryService } from '../../../manage-category/services/category.services';
import { Cuisine, cuisineService } from '../../../manage-cuisines/services/cuisine.service';
import { City, cityService } from '../../../manage-address/services/city.service';
import { Area, areaService } from '../../../manage-address/services/area.service';
class DropDown {

  constructor(public id: number = 0,
    public itemName: string = "") { }
}
@Component({
  selector: 'app-update-vendor',
  templateUrl: './update-vendor.component.html',
  styleUrls: ['./update-vendor.component.css']
})
export class UpdateVendorComponent implements OnInit {
  @Output() isBack: EventEmitter<boolean> = new EventEmitter<boolean>();
  tempareas: any;
  tempcities: any;
  tempcusines: any;
  tempcategories: any;
  dropcategory: boolean = false;
  temcategories: string[] = [];
  temcusines: string[] = [];
  templocations: string[]=[];
  categories: Category[]=[];
  cusines: Cuisine[]=[];
  cities: City[]=[];
  areas: Area[]=[];
  vendorDetail: VendorDetail;
  @Input() updateVendorDetail: VendorDetail[];
  @Input() updateId: number;
  @Output() updatedVendorDetail: EventEmitter<VendorDetail[]> = new EventEmitter<VendorDetail[]>();
  selectedCategoriesItems = [];
  dropdownCategoriesSettings = {};
  dropdownCategoriesObject: DropDown;
  dropdownCategoriesList: DropDown[] = []
  temp:DropDown[]=[]

  selectedCusinesItems = [];
  dropdownCusinesSettings = {};
  dropdownCusinesObject: DropDown;
  dropdownCusinesList: DropDown[] = []
  selectedCityItems = [];
  dropdownCitySettings = {};
  dropdownCityObject: DropDown;
  dropdownCityList: DropDown[] = []
  selectedAreaItems = [];
  dropdownAreaSettings = {};
  dropdownAreaObject: DropDown;
  dropdownAreaList: DropDown[] = []
  selectedlocationsItems = [];
  dropdownlocationSettings = {};
  dropdownlocationObject: DropDown;
  dropdownlocationList: DropDown[] = []

  constructor(public vendorDetailService: vendorDetailService, public areaServce: areaService, public categoryService: categoryService, public cuisineService: cuisineService, public cityService: cityService, public areaService: areaService) { }

  ngOnInit() {



    console.log("THIS ID WANT TO UPDTAE " + this.updateId)

    for (let i = 0; i < this.updateVendorDetail.length; i++) {
      if (this.updateVendorDetail[i]._id === this.updateId) {
        console.log("found object" + this.updateVendorDetail[i])
        this.vendorDetail = this.updateVendorDetail[i];

      }

    }
    console.log("THIS OBJECT IS GOING TO UPDATE")
    console.log(this.vendorDetail)
    this.categoryService.getCategoryList()
      .subscribe(res => {
        this.tempcategories=res; 
        for(let i =0;i<this.tempcategories.length;i++){
          if(this.tempcategories[i].categoryStatus==="Active"){
            this.categories.push(this.tempcategories[i]);
          }
        }
        
        console.log(this.categories);
       let index:number=0;
        
        for (let g = 0; g < this.categories.length; g++) {
          // console.log("i am in categoryservice loop");
          this.dropdownCategoriesObject = new DropDown(g, this.categories[g].name)
          this.dropdownCategoriesList.push(this.dropdownCategoriesObject)
        }
        for (let j = 0; j < this.vendorDetail.categories.length; j++) {

          for (let i = 0; i < this.dropdownCategoriesList.length; i++) {

            if (this.vendorDetail.categories[j] === this.dropdownCategoriesList[i].itemName) {
              
              this.dropcategory=true;
               this.selectedCategoriesItems[index] = new DropDown(i, this.dropdownCategoriesList[i].itemName);
              index++
            }



          }
          



        }

      });
    this.cuisineService.getCuisineList()
      .subscribe(res => {
        this.tempcusines=res; 
        for(let i =0;i<this.tempcusines.length;i++){
          if(this.tempcusines[i].cusineStatus==="Active"){
            this.cusines.push(this.tempcusines[i]);
          }
        }
       
        console.log(this.cusines);
        let index:number=0;
        for (let l = 0; l < this.cusines.length; l++) {
          // console.log("i am in cusine service lop");
          this.dropdownCusinesObject = new DropDown(l, this.cusines[l].name)
          this.dropdownCusinesList.push(this.dropdownCusinesObject)
        }
        for(let j =0;j<this.vendorDetail.cusines.length;j++){
          for (let i = 0; i < this.dropdownCusinesList.length; i++) {

              if(this.vendorDetail.cusines[j]===this.dropdownCusinesList[i].itemName ){
                console.log(this.dropdownCusinesList[i].itemName);
                this.selectedCusinesItems[index] = new DropDown(i, this.dropdownCusinesList[i].itemName);
                index++
              }
             }



          }

      });

    this.cityService.getCityList()
      .subscribe(response => {
       
        this.tempcities=response; 
        for(let i =0;i<this.tempcities.length;i++){
          if(this.tempcities[i].cityStatus==="Active"){
            this.cities.push(this.tempcities[i]);
          }
        }
        console.log(this.cities)
        for (let j = 0; j < this.cities.length; j++) {
          // console.log("i am in city service loop");
          this.dropdownCityObject = new DropDown(j, this.cities[j].name)
          this.dropdownCityList.push(this.dropdownCityObject)
        }

        for (let i = 0; i < this.dropdownCityList.length; i++) {

          if (this.vendorDetail.city === this.dropdownCityList[i].itemName) {
            console.log(this.dropdownCityList[i].itemName);
            this.selectedCityItems[0] = new DropDown(i, this.vendorDetail.city);
            // this.onItemSelect(this.selectedCityItems);

          }
        }




      });


    this.areaServce.getAreaList()
      .subscribe(response => {
        this.tempareas=response; 
        for(let i =0;i<this.tempareas.length;i++){
          if(this.tempareas[i].areaStatus==="Active"){
            this.areas.push(this.tempareas[i]);
          }
        }
      
        let index: number;
        let index2: number;
        index = 0;
        index2 = 0;
        for (let j = 0; j < this.areas.length; j++) {
          console.log("i am in area service loop");
          if (this.selectedCityItems.length > 0) {
            if (this.areas[j].cityName === this.selectedCityItems[0].itemName) {
              // this.selectedAreaItems=[]

              this.dropdownAreaObject = new DropDown(index, this.areas[j].name)
              this.dropdownAreaList.push(this.dropdownAreaObject)
              this.dropdownlocationList.push(this.dropdownAreaObject)
              index++;
            }
          }


         

        }
        for(let m =0;m<this.vendorDetail.deliveryLocation.length;m++){
          for (let n= 0; n < this.dropdownlocationList.length; n++) {

              if(this.vendorDetail.deliveryLocation[m]===this.dropdownlocationList[n].itemName ){
                console.log(this.dropdownlocationList[n].itemName);
                this.selectedlocationsItems[index2] = new DropDown(n, this.dropdownlocationList[n].itemName);
                index2++
              }
             }



          }
        for (let i = 0; i < this.dropdownAreaList.length; i++) {

          if (this.vendorDetail.area === this.dropdownAreaList[i].itemName) {
            console.log(this.dropdownAreaList[i].itemName);
            this.selectedAreaItems[0] = new DropDown(i, this.vendorDetail.area);
            // this.onItemSelect(this.selectedCityItems);

          }
        }



      });

  


    this.dropdownCategoriesSettings = {
      singleSelection: false,
      text: "Select Categories",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };
    this.dropdownCusinesSettings = {
      singleSelection: false,
      text: "Select Cusines",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };
    this.dropdownCitySettings = {
      singleSelection: true,
      text: "Select City",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };
    this.dropdownAreaSettings = {
      singleSelection: true,
      text: "Select Area",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };

    this.dropdownlocationSettings = {
      singleSelection: false,
      text: "Select Location",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };

  }
  onItemSelect(item: any) {
    console.log(item);
    console.log("selectedCategoriesItems" + this.selectedCategoriesItems);

    if (this.selectedCityItems.length > 0) {
      // console.log("city added");


      // console.log("i am in area service");
      console.log(this.areas)
      this.dropdownAreaList = [];
      this.dropdownlocationList=[];
      // this.selectedAreaItems=[]
      for (let j = 0; j < this.areas.length; j++) {
        // console.log("i am in area service loop");

        if (this.areas[j].cityName === this.selectedCityItems[0].itemName) {
          // this.selectedAreaItems=[]
          this.dropdownAreaObject = new DropDown(j, this.areas[j].name)
          this.dropdownAreaList.push(this.dropdownAreaObject)
          this.dropdownlocationList.push(this.dropdownAreaObject);
        }
        if (item.itemName === this.selectedCityItems[0].itemName) {
          // console.log("i am in deleting");

          this.selectedAreaItems = [];
          this.selectedlocationsItems=[]
        }

      }



    }
  }
  OnItemDeSelect(item: any) {
    console.log(item);


    if (this.selectedCityItems.length < 1) {
      this.dropdownAreaList = [];
      this.selectedAreaItems = []
      this.dropdownlocationList=[];
      this.selectedlocationsItems=[];
    }

    // console.log(this.selectedItems2);
  }
  onSelectAll(items: any) {

    console.log(items);
  }
  onDeSelectAll(items: any) {

    console.log(items);


  }
  update(vendorDetail): void {
    for (let i = 0; i < this.selectedCategoriesItems.length; i++) {

      this.temcategories[i] = this.selectedCategoriesItems[i].itemName
    }
    for (let i = 0; i < this.selectedCusinesItems.length; i++) {

      this.temcusines[i] = this.selectedCusinesItems[i].itemName
    }
    for (let i = 0; i < this.selectedlocationsItems.length; i++) {
      
            this.templocations[i] = this.selectedlocationsItems[i].itemName
          }
    console.log("hi i awan to update " + vendorDetail)
    console.log(vendorDetail)
    vendorDetail.categories = this.temcategories
    vendorDetail.cusines = this.temcusines;
    vendorDetail.deliveryLocation=this.templocations;
    vendorDetail.type="vendor"
    vendorDetail.city = this.selectedCityItems[0].itemName
    vendorDetail.area = this.selectedAreaItems[0].itemName
    this.vendorDetailService.updateVendorDetail(this.updateId, vendorDetail)
      .subscribe((response: Response) => {
        for (let i = 0; i < this.updateVendorDetail.length; i++) {
          if (this.updateVendorDetail[i]._id === this.vendorDetail._id) {

            this.updateVendorDetail[i].vendorName = vendorDetail.vendorName;
            this.updateVendorDetail[i].email = vendorDetail.email;
            // this.updateVendorDetail[i].password = vendorDetail.password;
            // this.updateVendorDetail[i].confirmPassword = vendorDetail.confirmPassword;
            // this.updateVendorDetail[i].userName = vendorDetail.userName;
            // this.updateVendorDetail[i].vendorSMSnumber = vendorDetail.vendorSMSnumber;
            // this.updateVendorDetail[i].contactAddress = vendorDetail.contactAddress;
         
            // this.updateVendorDetail[i].commision = vendorDetail.commision;


            // this.updateVendorDetail[i].categories = this.temcategories
            // this.updateVendorDetail[i].cusines = this.temcusines;
            // this.updatedVendorDetail[i].deliveryLocation=this.templocations;
            // this.updateVendorDetail[i].serviceTax = vendorDetail.serviceTax;
            // this.updateVendorDetail[i].minOrderValue = vendorDetail.minOrderValue;
            // this.updateVendorDetail[i].deliveryTime = vendorDetail.deliveryTime;
            // this.updateVendorDetail[i].paymentOption = vendorDetail.paymentOption;
            // this.updateVendorDetail[i].resturantType = vendorDetail.resturantType;
            // this.updateVendorDetail[i].deliveryCostAmount = vendorDetail.deliveryCostAmount;
            // this.updateVendorDetail[i].deliveryCostKM = vendorDetail.deliveryCostKM;
            // this.updateVendorDetail[i].deliveryAdditionalCostKM = vendorDetail.deliveryAdditionalCostKM;
            this.updateVendorDetail[i].vendorStatus = vendorDetail.vendorStatus;


          }
        }
      });




    console.log(this.updateVendorDetail)

    this.updatedVendorDetail.emit(this.updateVendorDetail);
  }
  back(): void {
    
    this.isBack.emit(false);

  }
}
