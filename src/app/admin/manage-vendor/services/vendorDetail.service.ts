import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { HttpClient } from '../../../shared/common/httpClient';
import { Headers, RequestOptions } from '@angular/http';
export class VendorDetail {
    public _id: number;
    public vendorName: string;
    public email:string
    public password:string
    public confirmPassword:string
    public userName:string
    public vendorSMSnumber:string
    public contactAddress:string
    public vendorCallNumber:string
    public categories:string[];
    public cusines:string[];
    public city:string
    public area:string
    public deliveryLocation:string[];
    public commision:number
    public serviceTax:string
    public minOrderValue:string
    public deliveryTime:string
    public paymentOption:string
    public resturantType:string
    public deliveryCostAmount:string
    public deliveryCostKM:string
    public deliveryAdditionalCostKM:string
    public vendorStatus:string;
    public type:string;
   
}
@Injectable()
export class vendorDetailService {
    
        constructor(private http: Http, private HttpClient: HttpClient) {
        }
    
        getVendorDetail(id: number) {
            return this.getVendorDetailList()
                .map(VendorDetails => VendorDetails.find(VendorDetail => VendorDetail.id === id))
                // .do(value => (console.log(value)))
                .catch(this.handlError);
        }
    
        getVendorDetailList() {
    
            // return this.http
            //     .get('assets/city.json')
            //     .map((response: Response) => <City[]>response.json().City)
            //     // .do(value => console.log(value))
            //     .catch(this.handlError);
            let url = "vendordetails";
            // console.log("sbvhk")
            return this.HttpClient
                .get(url)
                .map((response: Response) => {
                    let data = response.json();
                    return data;
                });
        }
        postVendorDetail(vendordetail: VendorDetail) {
            let url = "vendordetails"
            let headers = new Headers({ 'Content-Type': 'application/json' });
            let options = new RequestOptions({ headers: headers });
            return this.HttpClient
                .post(url, vendordetail)
                .map((response: Response) => {
                    let data = response.json();
                    return data;
                });
        }
        updateVendorDetail(id, vendordetail: VendorDetail) {
            let url = "vendordetails/" + id
            let headers = new Headers({ 'Content-Type': 'application/json' });
            let options = new RequestOptions({ headers: headers });
            return this.HttpClient
                .put(url, vendordetail)
                .map((response: Response) => {
                    let data = response.json();
                    return data;
                });
        }
        deleteVendorDetail(id) {
            console.log(id)
            let url = "vendordetails/" + id
            let headers = new Headers({ 'Content-Type': 'application/json' });
            let options = new RequestOptions({ headers: headers });
            return this.HttpClient
                .delete(url)
                .map((response: Response) => {
                    let data = response.json();
                    return data;
                });
        }
    
        private handlError(error: Response) {
            let msg = `Error status code${error.status} at ${error.url}`;
            return Observable.throw(msg);
        }
    }