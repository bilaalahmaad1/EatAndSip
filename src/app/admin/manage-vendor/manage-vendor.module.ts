import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageVendorComponent } from './manage-vendor/manage-vendor.component';
import { VendorDetailComponent } from './vendor-detail/vendor-detail.component';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { vendorDetailService } from './services/vendorDetail.service';
import { AddVendorComponent } from './vendor-detail/add-vendor/add-vendor.component';
import { UpdateVendorComponent } from './vendor-detail/update-vendor/update-vendor.component';
// import {DropDownCitiesComponent} from '../../shared/common/drop-down-cities/drop-down-cities.component';
import { SharedModule } from '../../shared/shared.module';
import { AngularMultiSelectModule } from '../../../../node_modules/angular2-multiselect-dropdown/angular2-multiselect-dropdown';







@NgModule({
  imports: [
    CommonModule,
    AngularMultiSelectModule,
    HttpModule,
    RouterModule,
    BrowserModule,
    SharedModule,
    FormsModule
  ],
  declarations: [ManageVendorComponent, VendorDetailComponent, AddVendorComponent, UpdateVendorComponent],
  exports: [ManageVendorComponent],
  providers: [vendorDetailService]
})
export class ManageVendorModule { }
