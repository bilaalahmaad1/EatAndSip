import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import {HttpClient} from '../../../shared/common/httpClient';
import {  Headers ,RequestOptions} from '@angular/http';


//model issue 
export class City {
    public _id: number; 
    public name: string; 
    public countryName: string
    cityStatus:string;
    // constructor() { }
}
//model issue 

@Injectable()
export class cityService {

    constructor(private http: Http,private HttpClient:HttpClient) {
    }

    getCity(id: number) {
        return this.getCityList()
            .map(Cities => Cities.find(City => City.id === id))
            // .do(value => (console.log(value)))
            .catch(this.handlError);
    }

    getCityList() {

        // return this.http
        //     .get('assets/city.json')
        //     .map((response: Response) => <City[]>response.json().City)
        //     // .do(value => console.log(value))
        //     .catch(this.handlError);
          let url = "cities";
            // console.log("sbvhk")
             return this.HttpClient
            .get(url)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
 postCity(city: City) {
        let url = "cities"
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .post(url, city)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
    updateCity(id,city:City){
          let url = "cities/"+id
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .put(url,city)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
  deleteCity(id) {
      console.log(id)
        let url = "cities/"+id
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .delete(url)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }

    private handlError(error: Response) {
        let msg = `Error status code${error.status} at ${error.url}`;
        return Observable.throw(msg);
    }
}