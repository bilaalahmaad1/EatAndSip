import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {HttpClient} from '../../../shared/common/httpClient';
import {  Headers ,RequestOptions} from '@angular/http';
import 'rxjs/Rx';


//model issue 
export class Country {
    public _id: number; 
    public name: string; 
    public ISOcode: string
    countryStatus:string
    // constructor() { }
}
//model issue 

@Injectable()
export class countryService {

    constructor(private http: Http,private HttpClient:HttpClient) {
    }

    getCountry(id: number) {
        return this.getCountryList()
            .map(Countries => Countries.find(Country => Country.id === id))
            // .do(value => (console.log(value)))
            .catch(this.handlError);
    }

    getCountryList() {

        // return this.http
        //     .get('assets/country.json')
        //     .map((response: Response) => <Country[]>response.json().Country)
        //     // .do(value => console.log(value))
        //     .catch(this.handlError);
         let url = "countries";
            // console.log("sbvhk")
             return this.HttpClient
            .get(url)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
           
    }
    postCountry(country: Country) {
        let url = "countries"
        // let headers = new Headers({ 'Content-Type': 'application/json' });
        // let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .post(url, country)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
    updateCountry(id,country:Country){
          let url = "countries/"+id
        // let headers = new Headers({ 'Content-Type': 'application/json' });
        // let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .put(url,country)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
  deleteCountry(id) {
      console.log("id is "+id)
        let url = "countries/"+id
      //  let headers = new Headers({ 'Content-Type': 'application/json' });
      //  let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .delete(url)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }

    private handlError(error: Response) {
        let msg = `Error status code${error.status} at ${error.url}`;
        return Observable.throw(msg);
    }
}