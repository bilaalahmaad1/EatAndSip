import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {HttpClient} from '../../../shared/common/httpClient';
import { Headers ,RequestOptions} from '@angular/http';
import 'rxjs/Rx';


//model issue 
export class Area {
    public _id: number; 
    public name: string; 
    public countryName: string;
    public cityName :string;
    public areaStatus :string;
    // constructor() { }
}
//model issue 

@Injectable()
export class areaService {

    constructor(private http: Http,private HttpClient:HttpClient) {
    }

    getArea(id: number) {
        return this.getAreaList()
            .map(Areas => Areas.find(Area => Area.id === id))
            // .do(value => (console.log(value)))
            .catch(this.handlError);
    }

    getAreaList() {

        // return this.http
        //     .get('assets/city.json')
        //     .map((response: Response) => <City[]>response.json().City)
        //     // .do(value => console.log(value))
        //     .catch(this.handlError);
          let url = "areas";
            // console.log("sbvhk")
             return this.HttpClient
            .get(url)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
 postArea(area: Area) {
        let url = "areas"
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .post(url, area)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
    updateArea(id,area:Area){
          let url = "areas/"+id
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .put(url,area)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
  deleteArea(id) {
      console.log(id)
        let url = "areas/"+id
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .delete(url)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }

    private handlError(error: Response) {
        let msg = `Error status code${error.status} at ${error.url}`;
        return Observable.throw(msg);
    }
}