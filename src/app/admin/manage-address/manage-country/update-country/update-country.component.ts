import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { countryService, Country } from '../../services/country.service';
import { Area, areaService } from '../../services/area.service';
import { City, cityService } from '../../services/city.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// import {VendorDetail,vendorDetailService} from '../../../manage-vendor/services/vendorDetail.service';

import { Http, Response } from '@angular/http';

@Component({
  selector: 'app-update-country',
  templateUrl: './update-country.component.html',
  styleUrls: ['./update-country.component.css']
})
export class UpdateCountryComponent implements OnInit {
  myform: FormGroup;
  areas: Area[];
  cities: City[];
  isStatus: any;
  country: Country;
  tempcountry:string
  @Input() updateCountry: Country[];
  @Input() updateId: number;
  @Output() updatedCountry: EventEmitter<Country[]> = new EventEmitter<Country[]>();
  @Output() isBack: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(public countryService: countryService, public areaService: areaService, public cityService: cityService, private router: Router, private route: ActivatedRoute, private fb: FormBuilder) { 
    this.tempcountry="";


    this.myform = this.fb.group({
      cname: [null, Validators.required],
      cisocode: [null, Validators.required],
      status: [null, Validators.required],
    });
  }

  ngOnInit() {

    this.cityService.getCityList().subscribe(res => { this.cities = res });
    this.areaService.getAreaList().subscribe(res => { this.areas = res });
   

    for (let i = 0; i < this.updateCountry.length; i++) {
      if (this.updateCountry[i]._id === this.updateId) {
        // console.log("found object"+this.updateCountry[i] )
        this.country = this.updateCountry[i];
        this.tempcountry=this.country.name
      }

    }




  }
  update(country): void {
    console.log("hi i awan to update " + country)
    // country.countryStatus = this.isStatus;
    console.log(country)
    this.countryService.updateCountry(this.updateId, country)
      .subscribe((response: Response) => {
        for(let j =0;j<this.cities.length;j++){
          if(this.cities[j].countryName===this.tempcountry){
           this.cities[j].countryName=country.name;
            this.cityService.updateCity(this.cities[j]._id, this.cities[j]).subscribe();
   
          }
        }
        for(let k =0;k<this.areas.length;k++){
          if(this.areas[k].countryName===this.tempcountry){
           this.areas[k].countryName=country.name;
            this.areaService.updateArea(this.areas[k]._id, this.areas[k]).subscribe();
   
          }
        }
       
        

        for (let i = 0; i < this.updateCountry.length; i++) {
          if (this.updateCountry[i]._id === this.country._id) {
            this.updateCountry[i].name = country.name
            this.updateCountry[i].ISOcode = country.ISOcode

          }
        }

      });




    console.log("THIS OBJECT IS NOW UPDATED")
    console.log(this.updateCountry);

    this.updatedCountry.emit(this.updateCountry);
  }
  back(): void {
    
    this.isBack.emit(false);

  }
  status(country,isStatus) {
    this.isStatus = isStatus;
    if (country.countryStatus === "Active") {
      for (let i = 0; i < this.cities.length; i++) {
        if (this.cities[i].countryName === country.name) {
          this.cities[i].cityStatus = "Active"
          //  console.log(this.cities[i]);
          this.cityService.updateCity(this.cities[i]._id, this.cities[i])
            .subscribe(res => {
              console.log(res);
            });
        }
      }
      for (let j = 0; j < this.areas.length; j++) {
        if (this.areas[j].countryName === country.name) {
          this.areas[j].areaStatus = "Active"
          //  console.log(this.cities[i]);
          this.areaService.updateArea(this.areas[j]._id, this.areas[j])
            .subscribe(res => {
              console.log(res);
            });
        }
      }

    }
    if (country.countryStatus === "Deactive") {
      for (let i = 0; i < this.cities.length; i++) {
        if (this.cities[i].countryName === country.name) {
          this.cities[i].cityStatus = "Deactive"
          //  console.log(this.cities[i]);
          this.cityService.updateCity(this.cities[i]._id, this.cities[i])
            .subscribe(res => {
              console.log(res);
            });
        }
      }
      for (let j = 0; j < this.areas.length; j++) {
        if (this.areas[j].countryName === country.name) {
          this.areas[j].areaStatus = "Deactive"
          //  console.log(this.cities[i]);
          this.areaService.updateArea(this.areas[j]._id, this.areas[j])
            .subscribe(res => {
              console.log(res);
            });
        }
      }

    }
  }
}
