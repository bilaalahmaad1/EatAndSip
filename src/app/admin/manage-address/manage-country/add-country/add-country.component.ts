import { NgModule } from '@angular/core';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Country } from '../../services/country.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-add-country',
  templateUrl: './add-country.component.html',
  styleUrls: ['./add-country.component.css']
})
export class AddCountryComponent implements OnInit {
  myform: FormGroup;

  @Output() country: EventEmitter<Country> = new EventEmitter<Country>();
  @Output() isBack: EventEmitter<boolean> = new EventEmitter<boolean>();
  newCountry: Country;
  isStatus: any;
  constructor(private router: Router, private fb: FormBuilder) {
    this.newCountry = new Country();
    this.isStatus = "";


    this.myform = this.fb.group({
      cname: [null, Validators.required],
      cisocode: [null, Validators.required],
      status: [null, Validators.required],
    });

  }


  ngOnInit() {
  }
  save(): void {
    // back to country with country
    // console.log(this.newCountry)
    this.newCountry.countryStatus = this.isStatus;
    console.log(this.newCountry);

    this.country.emit(this.newCountry);
  }
  back(): void {
    // back to country with empty country
    // this.country.emit(this.newCountry);
    this.isBack.emit(true);

  }
  status(isStatus) {
    this.isStatus = isStatus;
  }

}
