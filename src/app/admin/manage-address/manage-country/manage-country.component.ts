import { Component, OnInit, Input } from '@angular/core';
import { countryService, Country } from '../services/country.service';
import { Area, areaService } from '../services/area.service';
import { City, cityService } from '../services/city.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-manage-country',
  templateUrl: './manage-country.component.html',
  styleUrls: ['./manage-country.component.css']
})
export class ManageCountryComponent implements OnInit {
  areas: Area[];
  cities: City[];
  countries: Country[];
  isCreate: boolean;
  isAdd: boolean;
  isUpdate: boolean;
  id: number;
  resCountry: Country;
  mesg: string;
  constructor(
    public countryservice: countryService,
    public cityService: cityService,
    public areaService: areaService,
    private router: Router) {
    this.countries = [];
    this.isCreate = true;
    this.isAdd = false;
    this.isUpdate = false;
    this.mesg = "";

  }

  ngOnInit() {
    this.getCountries();
    this.cityService.getCityList().subscribe(res => { this.cities = res });
    this.areaService.getAreaList().subscribe(res => { this.areas = res });
  }
  getCountries(): void {
    this.countryservice.getCountryList()
      .subscribe(responce => {
        if (responce.success === false) { this.router.navigate(['/login']); }
        this.countries = responce;

      });
  }
  createCountry(): void {
    console.log("create button hits");
    this.isAdd = true;
    this.isCreate = false;
    // this.router.navigate(['/Country/Add']); 
  }
  addCountry(country: Country) {
    console.log("I am in addCountry function new country comes from add component");
    console.log(country);
    this.isAdd = false;
    this.isCreate = true;

    //add post method of service to send data in data base 
    console.log("we are adding countru to servise to db");
    this.countryservice.postCountry(country)
      .subscribe(responce => {
        this.resCountry = responce
        //  console.log("log1")
        console.log("after adding new country to db we get this response");
        console.log(this.resCountry)
        //  console.log("log2")
        this.countries.push(this.resCountry);
        // console.log(this.countries);
      });



  }
  delete(id: Number): void {
    // console.log(id);
    // this.getCountries();
    this.countryservice.deleteCountry(id)
      .subscribe(responce => {
        this.mesg = responce
        //  console.log(this.mesg);
        for (let i = 0; i < this.countries.length; i++) {
          if (id === this.countries[i]._id) {
            let index;
            index = i;
            console.log(this.countries[i].name)
            console.log(this.cities);
            for (let j = 0; j < this.cities.length; j++) {
              if (this.cities[j].countryName === this.countries[i].name) {
                // console.log(this.cities[j])
                this.cityService.deleteCity(this.cities[j]._id).subscribe(res => { console.log(res) })
              }
            }
            for (let k = 0; k < this.areas.length; k++) {
              if (this.areas[k].countryName === this.countries[i].name) {
                console.log(this.areas[k]);
                this.areaService.deleteArea(this.areas[k]._id).subscribe(res => { console.log(res) })
              }
            }


            this.countries.splice(index, 1)
            this.countries = this.countries;

          }
        }
      });


  }
  update(id): void {
    console.log("HIT UPDATE BUTTON");
    this.isCreate = false;
    this.isUpdate = true;
    this.id = id
    //  this.router.navigate(['Country/Update',id]); 
  }
  updateCountry(updatedCountries: Country[]): void {
    this.countries = updatedCountries;
    console.log("i am in updateCountry function")
    console.log(this.countries)
    this.isCreate = true;
    this.isUpdate = false;
  }
  showCountriesPage(condition: boolean) {
    this.getCountries();
    if (condition === true) {
      this.isAdd = false;
      this.isCreate = true;
    }
    else {
      this.isCreate = true;
      this.isUpdate = false;
    }

  }

}

