import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { cityService, City } from '../../services/city.service';
import { Country, countryService } from '../../services/country.service';
import { Area, areaService } from '../../services/area.service';
import { Http, Response } from '@angular/http';
import {VendorDetail,vendorDetailService} from '../../../manage-vendor/services/vendorDetail.service';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-update-city',
  templateUrl: './update-city.component.html',
  styleUrls: ['./update-city.component.css']
})
export class UpdateCityComponent implements OnInit {
  myform: FormGroup;
  tempcountries: any;
  vendors: VendorDetail[];
  areas: Area[];
  isCountry: any;
  countries: Country[]=[];
  tempcity:string;
  city: City;
  isStatus: any;
  @Input() updateCity: City[];
  @Input() updateId: number;
  @Output() updatedCity: EventEmitter<City[]> = new EventEmitter<City[]>();
  @Output() isBack: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor(public vendorDetailService:vendorDetailService,private fb: FormBuilder,public cityservice: cityService , public countryService:countryService, public areaService:areaService) {
    this.tempcity="";
    this.myform = this.fb.group({
      cname: [null, Validators.required],
      scountry: [null, Validators.required],
      status: [null, Validators.required],
    });
    
   }

  ngOnInit() {


    this.areaService.getAreaList().subscribe(res => { this.areas = res });

    this.vendorDetailService.getVendorDetailList().subscribe(res => { this.vendors = res });
    for (let i = 0; i < this.updateCity.length; i++) {
      if (this.updateCity[i]._id === this.updateId) {
        console.log("found object" + this.updateCity[i])
        this.city = this.updateCity[i];
        this.tempcity=this.city.name;

      }

    }
    this.countryService.getCountryList()
    .subscribe(response=>{ this.tempcountries=response; 
        for(let i =0;i<this.tempcountries.length;i++){
          if(this.tempcountries[i].countryStatus==="Active"){
            this.countries.push(this.tempcountries[i]);
          }
        }

     
    });
    console.log(this.countries)
  }


  update(city): void {

    this.cityservice.updateCity(this.updateId, city)
      .subscribe((response: Response) => {
        console.log(response);
       

        for(let k =0;k<this.areas.length;k++){
          if(this.areas[k].cityName===this.tempcity){
           this.areas[k].cityName=city.name;
            this.areaService.updateArea(this.areas[k]._id, this.areas[k]).subscribe();
   
          }
        }
        for(let a =0;a<this.vendors.length;a++){
          if(this.vendors[a].city===this.tempcity){
           this.vendors[a].city=city.name;
            this.vendorDetailService.updateVendorDetail(this.vendors[a]._id, this.vendors[a]).subscribe();
   
          }
        }


        for (let i = 0; i < this.updateCity.length; i++) {
          if (this.updateCity[i]._id === this.updateId) {
            this.updateCity[i].name = this.city.name
            this.updateCity[i].countryName = this.city.countryName
          }
        }
      });
    console.log(this.updateCity);
    this.updatedCity.emit(this.updateCity);
  }
  back(): void {
    // back to country with empty country
    // this.country.emit(this.newCountry);
    this.isBack.emit(false);

  }
  status(city,isStatus) {
    this.isStatus = isStatus;
    if (city.cityStatus === "Active") {
      
      for (let j = 0; j < this.areas.length; j++) {
        if (this.areas[j].cityName === city.name) {
          this.areas[j].areaStatus = "Active"
          //  console.log(this.cities[i]);
          this.areaService.updateArea(this.areas[j]._id, this.areas[j])
            .subscribe(res => {
              console.log(res);
            });
        }
      }

    }
    if (city.cityStatus === "Deactive") {
     
      for (let j = 0; j < this.areas.length; j++) {
        if (this.areas[j].cityName === city.name) {
          this.areas[j].areaStatus = "Deactive"
          //  console.log(this.cities[i]);
          this.areaService.updateArea(this.areas[j]._id, this.areas[j])
            .subscribe(res => {
              console.log(res);
            });
        }
      }

    }
  }
  
  // selectedCity(countryName){
  //   this.isCountry=countryName

  // }
}

