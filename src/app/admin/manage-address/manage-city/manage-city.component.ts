import { Component, OnInit } from '@angular/core';
import { cityService, City } from '../services/city.service';
import { Area, areaService } from '../services/area.service';
import {Country,countryService} from '../services/country.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-manage-city',
  templateUrl: './manage-city.component.html',
  styleUrls: ['./manage-city.component.css']
})
export class ManageCityComponent implements OnInit {


  countries: Country[];
  areas: Area[];
  cities: City[];
  isCreate: boolean;
  isAdd: boolean;
  isUpdate: boolean;
  id: number;
  resCity: City;
  mesg: string;
  constructor(public cityservice: cityService,public countryService: countryService,
    public areaService: areaService , public router:Router) {
    this.cities = [];
    this.isCreate = true;
    this.isAdd = false;
    this.isUpdate = false;
    this.mesg = "";
  }

  ngOnInit() {
    this.getCities();
    this.areaService.getAreaList().subscribe(res => { this.areas = res });
    this.countryService.getCountryList().subscribe(res=>{this.countries=res});
    
  }
  getCities(): void {
    this.cityservice.getCityList()
      .subscribe(responce => {
        if (responce.success === false) { this.router.navigate(['/login']);}
        this.cities = responce
        console.log(this.cities)
      });
  }
  createCity(): void {
    this.isAdd = true;
    this.isCreate = false;
    // this.router.navigate(['/Country/Add']); 
  }
  addCity(city: City) {
    console.log("I am in addCity function");
    this.isAdd = false;
    this.isCreate = true;
    // console.log(city);
    // this.cities.push(city);
    // console.log(this.cities);
    this.cityservice.postCity(city)
      .subscribe(responce => {
        this.resCity = responce
        //  console.log("log1")
        console.log("after adding new city to db we get this response");
        console.log(this.resCity)
        //  console.log("log2")
        this.cities.push(this.resCity);
      });

  }
  delete(id: Number): void {

    // console.log(id);
    this.cityservice.deleteCity(id)
      .subscribe(responce => {
        this.mesg = responce
        console.log(this.mesg);
        for (let i = 0; i < this.cities.length; i++) {
          if (id === this.cities[i]._id) {
            let index;
            index = i;
            for(let j =0;j<this.areas.length;j++){
              if(this.areas[j].cityName===this.cities[i].name){
                 console.log(this.areas[j].name)
                this.areaService.deleteArea(this.areas[j]._id).subscribe(res=>{console.log(res)})
              }
            }
            this.cities.splice(index, 1)
            
            this.cities = this.cities;
           
          }
        }
      });
  }
  update(city,id): void {
     for(let i =0;i<this.countries.length;i++){
       if(city.countryName===this.countries[i].name){
         if(this.countries[i].countryStatus==="Active")
         {
          this.isCreate = false;
          this.isUpdate = true;
          this.id = id
         }
         else{
           console.log("activate it country first ");

         }
       }
     }

   
    //  this.router.navigate(['Country/Update',id]); 
  }
  updateCity(updatedCountries: City[]): void {
    this.cities = updatedCountries;
    console.log("i am in updateCity function")
    console.log(this.cities)
    this.isCreate = true;
    this.isUpdate = false;
  }
  showCitiesPage(condition: boolean) {
    this.getCities();
    if (condition === true) {
      this.isAdd = false;
      this.isCreate = true;
    }
    else {
      this.isCreate = true;
      this.isUpdate = false;
    }

  }
}
