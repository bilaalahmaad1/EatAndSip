import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { City } from '../../services/city.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-add-city',
  templateUrl: './add-city.component.html',
  styleUrls: ['./add-city.component.css']
})
export class AddCityComponent implements OnInit {
 @Output() city: EventEmitter<City> = new EventEmitter<City>();
 @Output () isBack: EventEmitter<boolean> = new EventEmitter<boolean>();
 myform: FormGroup;
 isStatus:any;
  newCity: City;
  constructor(private fb: FormBuilder) {
    this.newCity = new City();

    this.myform = this.fb.group({
      cname: [null, Validators.required],
      // scountry: [null, Validators.required],
      status: [null, Validators.required],
    });
   }

  ngOnInit() {
  }
   save(): void {
    // back to country with country
    console.log(this.newCity)
    this.newCity.cityStatus=this.isStatus;
    this.city.emit(this.newCity);
  }
  back(): void {
    // back to country with empty country
    this.isBack.emit(true);

  }
  status(isStatus){
    this.isStatus=isStatus;
  }

}
