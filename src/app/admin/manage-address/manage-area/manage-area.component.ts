import { Component, OnInit } from '@angular/core';
import {Area,areaService} from '../services/area.service';
import {City,cityService} from '../services/city.service';
import {Country,countryService} from '../services/country.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-manage-area',
  templateUrl: './manage-area.component.html',
  styleUrls: ['./manage-area.component.css']
})
export class ManageAreaComponent implements OnInit {
  cities: City[];
  countries: Country[];
  areas: Area[];
  isCreate: boolean;
  isAdd: boolean;
  isUpdate: boolean;
  id:number;
  resArea:Area;
  mesg :string;
  constructor(public areaservice:areaService,
    public countryservice: countryService,
    public cityService: cityService,
    public router:Router
    ) { 
       this.areas = [];
    this.isCreate = true;
    this.isAdd = false;
    this.isUpdate = false;
    this.mesg="";
  }

  ngOnInit() {
     this.getAras();
     this.cityService.getCityList().subscribe(res => { this.cities = res });
     this.countryservice.getCountryList().subscribe(res=>{this.countries=res});
  }
    getAras(): void {
    this.areaservice.getAreaList()
      .subscribe(responce => {
        if (responce.success === false) { this.router.navigate(['/login']);}
        this.areas = responce
        console.log(this.areas)
      });
    }
     createArea(): void {
    this.isAdd = true;
    this.isCreate = false;
    // this.router.navigate(['/Country/Add']); 
  }
  addArea(area: Area) {
    console.log("I am in addArea function");
    this.isAdd = false;
    this.isCreate = true;
   
    // console.log(this.cities);
     this.areaservice.postArea(area)
    .subscribe(responce => {
        this.resArea = responce
      //  console.log("log1")
      console.log("after adding new area to db we get this response");
        console.log(this.resArea)
        //  console.log("log2")
    this.areas.push(this.resArea);
     });

  }
  delete(id: Number): void {
    // use load ash here 
    //  let index=_.findIndex(this.countries, function(o) { return n == id; });
    console.log(id);
    this.areaservice.deleteArea(id)
    .subscribe(responce => {
        this.mesg = responce
        console.log(this.mesg);
    for (let i = 0; i < this.areas.length; i++) {
      if (id === this.areas[i]._id) {
        let index;
        index = i;
        this.areas.splice(index, 1)
        // console.log(this.cities)
        this.areas = this.areas;
        //  this.newemployeedata=this.delEmployee
        // console.log(this.cities)
        // console.log(id)
        
      }
    }
     });
  }
  update(area,id):void{
    for(let i =0;i<this.countries.length;i++){
      if(area.countryName===this.countries[i].name){
        if(this.countries[i].countryStatus==="Active")
        {  for(let j =0;j<this.cities.length;j++){
          if(area.cityName===this.cities[j].name){
            if(this.cities[j].cityStatus==="Active")
            {
              this.isCreate = false;
              this.isUpdate = true;
              this.id = id
            }else{
              console.log("activate it city first ");
            }
          }
        }

          
         
        }
        else{
          console.log("activate it country first ");

        }
      }
    }
    
    //  this.router.navigate(['Country/Update',id]); 
  }
  updateArea(updatedCountries:Area[]):void{
    this.areas=updatedCountries;
    console.log("i am in updateArea function")
    console.log(this.areas)
      this.isCreate=true;
    this.isUpdate=false;
  }
  showAreaPage(condition:boolean){
    this.getAras();
if(condition===true){
  this.isAdd = false;
  this.isCreate = true;
}
else{
  this.isCreate=true;
  this.isUpdate=false;
}

  }

}
