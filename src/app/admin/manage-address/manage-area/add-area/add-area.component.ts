import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Area } from '../../services/area.service';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-add-area',
  templateUrl: './add-area.component.html',
  styleUrls: ['./add-area.component.css']
})
export class AddAreaComponent implements OnInit {

 @Output() area: EventEmitter<Area> = new EventEmitter<Area>();
 @Output () isBack: EventEmitter<boolean> = new EventEmitter<boolean>();
 myform: FormGroup;
 isStatus:any;
  newArea: Area;
  constructor(private fb: FormBuilder) {
    this.newArea = new Area();
    this.myform = this.fb.group({
      aname: [null, Validators.required],
      // scountry: [null, Validators.required],
      status: [null, Validators.required],
    });
   }

  ngOnInit() {
  }
   save(): void {
    // back to country with country
    console.log(this.newArea)
    this.newArea.areaStatus=this.isStatus;
    this.area.emit(this.newArea);
  }
  back(): void {
    // back to country with empty country
    this.isBack.emit(true);

  }
  status(isStatus){
    this.isStatus=isStatus;
  }


}