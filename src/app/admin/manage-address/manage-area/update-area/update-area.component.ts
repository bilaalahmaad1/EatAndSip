import { Component, OnInit , Input, Output, EventEmitter } from '@angular/core';
import { Country, countryService } from '../../services/country.service';
import {City,cityService} from '../../services/city.service';

import { Area,areaService } from '../../services/area.service';
import { Http, Response } from '@angular/http';
import {VendorDetail,vendorDetailService} from '../../../manage-vendor/services/vendorDetail.service';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-update-area',
  templateUrl: './update-area.component.html',
  styleUrls: ['./update-area.component.css']
})
export class UpdateAreaComponent implements OnInit {
  myform: FormGroup;
  tempcountries: any;
  tempcities: any;
  temparea: string;
  isStatus: any;
  vendors: VendorDetail[];
  area: Area;
  countries: Country[]=[];
  cities:City[]=[];
  @Input() updateArea: Area[];
  @Input() updateId: number;
  @Output() updatedArea: EventEmitter<Area[]> = new EventEmitter<Area[]>();
  @Output() isBack: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor(public vendorDetailService:vendorDetailService,private fb: FormBuilder,public areaservice: areaService, public countryService:countryService,public cityService:cityService ) {
    this.vendors=[];
    this.myform = this.fb.group({
      aname: [null, Validators.required],
      scountry: [null, Validators.required],
      scity: [null, Validators.required],
      status: [null, Validators.required],
    });
   }

  ngOnInit() {
    this.vendorDetailService.getVendorDetailList().subscribe(res=>{this.vendors=res})
     for (let i = 0; i < this.updateArea.length; i++) {
      if (this.updateArea[i]._id === this.updateId) {
        console.log("found object"+this.updateArea[i] )
        this.area=this.updateArea[i] ;
        this.temparea=this.area.name;
        
      }
  }
  this.countryService.getCountryList()
  .subscribe(response=>{ this.tempcountries=response; 
      for(let i =0;i<this.tempcountries.length;i++){
        if(this.tempcountries[i].countryStatus==="Active"){
          this.countries.push(this.tempcountries[i]);
        }
      }

   
  });
  this.cityService.getCityList()
  .subscribe(response=>{this.tempcities=response;
    for(let h =0;h<this.tempcities.length;h++){
      if(this.tempcities[h].cityStatus==="Active"){
        this.cities.push(this.tempcities[h]);
      }
    }
  });
// console.log(this.countries)

}
 update(area): void {
    this.areaservice.updateArea(this.updateId,area)
.subscribe((response:Response)=>{
    console.log(response);

    for(let a =0;a<this.vendors.length;a++){
      if(this.vendors[a].area===this.temparea){
       this.vendors[a].area=area.name;
        this.vendorDetailService.updateVendorDetail(this.vendors[a]._id, this.vendors[a]).subscribe();

      }
    }
    for (let i = 0; i < this.updateArea.length; i++) {
      if (this.updateArea[i]._id === this.updateId) {
        this.updateArea[i].name = this.area.name
        this.updateArea[i].countryName = this.area.countryName
      }
    }
    });
    console.log(this.updateArea);
    this.updatedArea.emit(this.updateArea);
  }
  back(): void {
    // back to country with empty country
    // this.country.emit(this.newCountry);
    this.isBack.emit(false);

  }
  status(isStatus) {
    this.isStatus = isStatus;
  }
}