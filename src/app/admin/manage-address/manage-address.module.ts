import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageAddressComponent } from './manage-address/manage-address.component';
import { ManageCountryComponent } from './manage-country/manage-country.component';
import { ManageCityComponent } from './manage-city/manage-city.component';
import { ManageAreaComponent } from './manage-area/manage-area.component';
import { RouterModule } from '@angular/router';
import { countryService } from './services/country.service';
import { HttpModule } from '@angular/http';
import { AddCountryComponent } from './manage-country/add-country/add-country.component';
import { UpdateCountryComponent } from './manage-country/update-country/update-country.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddCityComponent } from './manage-city/add-city/add-city.component';
import { UpdateCityComponent } from './manage-city/update-city/update-city.component';
import { cityService } from './services/city.service';
import { HttpClient } from '../../shared/common/httpClient';
// import { DropDownCoutriesComponent } from '../../shared/common/drop-down-coutries/drop-down-coutries.component';
// import { DropDownCitiesComponent } from '../../shared/common/drop-down-cities/drop-down-cities.component';
import { areaService } from './services/area.service';
import { AddAreaComponent } from './manage-area/add-area/add-area.component';
import { UpdateAreaComponent } from './manage-area/update-area/update-area.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [countryService, cityService, areaService, HttpClient],

  declarations: [ManageAddressComponent,
    ManageCountryComponent,
    ManageCityComponent, ManageAreaComponent,
    AddCountryComponent, UpdateCountryComponent,
    AddCityComponent, UpdateCityComponent, AddAreaComponent,
    UpdateAreaComponent],
  exports: [ManageAddressComponent, ManageCityComponent, ManageAreaComponent],
})
export class ManageAddressModule { }
