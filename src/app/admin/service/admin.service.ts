import { Injectable } from '@angular/core';
import {  Response } from '@angular/http';
 import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
// import { LoginModel } from '../model/login.model'
// import { ErrorObservable } from "rxjs/observable/ErrorObservable";
// import { Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '../../shared/common/httpClient'

@Injectable()
export class adminService {
    constructor( public httpclient: HttpClient) { }

    getAdmin() {
        let url = "admin";
        return this.httpclient
            .get(url)
            .map((response: Response) => {
                 console.log("membership service")
                let data = response.json();
                return data;
            });
    }
    private handlError(error: Response) {
        let msg = `Error status code${error.status} at ${error.url}`;
        return Observable.throw(msg);
    }
}
