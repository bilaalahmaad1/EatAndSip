import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Http, Response } from '@angular/http';
import { subCategory,subcategoryService } from '../../services/subCategory.service';
import {categoryService,Category}from '../../services/category.services';
@Component({
  selector: 'app-update-sub-category',
  templateUrl: './update-sub-category.component.html',
  styleUrls: ['./update-sub-category.component.css']
})
export class UpdateSubCategoryComponent implements OnInit {
  tempcategories: any;
  categories: Category[]=[];
  subcategory: subCategory;
  @Input() updatesubCategory: subCategory[];
  @Input() updateId: number;
  @Output() updatedsubCategory: EventEmitter<subCategory[]> = new EventEmitter<subCategory[]>();
  @Output() isBack: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor(public subcategoryService: subcategoryService,public categoryService: categoryService) { }

  ngOnInit() {
    this.categoryService.getCategoryList()
    .subscribe(response=>{ this.tempcategories=response; 
        for(let i =0;i<this.tempcategories.length;i++){
          if(this.tempcategories[i].categoryStatus==="Active"){
            this.categories.push(this.tempcategories[i]);
          }
        }

     
    });
    for (let i = 0; i < this.updatesubCategory.length; i++) {
      if (this.updatesubCategory[i]._id === this.updateId) {
        console.log("found object" + this.updatesubCategory[i])
        this.subcategory = this.updatesubCategory[i];

      }
    }
  }

  update(subcategory): void {
    console.log(subcategory); 
    this.subcategoryService.updatesubCategory(this.updateId, subcategory)
      .subscribe((response: Response) => {
        console.log(response);
        for (let i = 0; i < this.updatesubCategory.length; i++) {
          if (this.updatesubCategory[i]._id === this.updateId) {
            this.updatesubCategory[i].name = this.subcategory.name
            this.updatesubCategory[i].parentCategory=this.subcategory.parentCategory
           
          }
        }
      });

    this.updatedsubCategory.emit(this.updatesubCategory);
  }
  back(): void {
    // back to country with empty country
    // this.country.emit(this.newCountry);
    this.isBack.emit(false);

  }

}


