import { Component, OnInit ,Output, EventEmitter} from '@angular/core';
import {subCategory} from '../../services/subCategory.service'; 

@Component({
  selector: 'app-add-sub-category',
  templateUrl: './add-sub-category.component.html',
  styleUrls: ['./add-sub-category.component.css']
})
export class AddSubCategoryComponent implements OnInit {
@Output() subcategory: EventEmitter<subCategory> = new EventEmitter<subCategory>();
  newsubCategory: subCategory;
  isStatus:any;
  @Output () isBack: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor() { 
     this.newsubCategory = new subCategory();
  }

  ngOnInit() {
  }
  save(): void {
    // back to country with country
    console.log(this.newsubCategory)
    this.newsubCategory.sucCategoryStatus=this.isStatus;
    this.subcategory.emit(this.newsubCategory);
  }
  back(): void {
    // back to country with empty country
    this.isBack.emit(true);

  }
  status(isStatus){
    this.isStatus=isStatus;
  }


}
