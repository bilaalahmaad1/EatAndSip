import { Component, OnInit } from '@angular/core';
import { subCategory,subcategoryService} from '../services/subCategory.service'; 
import {Category,categoryService} from '../services/category.services';
import { Router } from '@angular/router';


@Component({
  selector: 'app-sub-category',
  templateUrl: './sub-category.component.html',
  styleUrls: ['./sub-category.component.css']
})
export class SubCategoryComponent implements OnInit {
  categories: Category[];
  subcategories: subCategory[];
  isCreate: boolean;
  isAdd: boolean;
  isUpdate: boolean;
  id:number;
 ressubCategory:subCategory;
 mesg :string;
  constructor(public subcategoryService:subcategoryService,public categoryService:categoryService,public router:Router) {
     this.subcategories = [];
    this.isCreate = true;
    this.isAdd = false;
    this.isUpdate = false;
    this.mesg="";
   }

  ngOnInit() {
    this.categoryService.getCategoryList().subscribe(res=>{this.categories=res})
    this.getsubCategories();
  }
   getsubCategories(): void {
    this.subcategoryService.getsubCategoryList()
      .subscribe(responce => {
        if (responce.success === false) { this.router.navigate(['/login']);}
        this.subcategories = responce
        console.log(this.subcategories)
      });
  }
    createsubCategory(): void {
    this.isAdd = true;
    this.isCreate = false;
   
  }
  addsubCategory(subcategory: subCategory) {
    console.log("I am in addsubCategory function");
    this.isAdd = false;
    this.isCreate = true;
   
     this.subcategoryService.postsubCategory(subcategory)
    .subscribe(responce => {
        this.ressubCategory = responce
    console.log(this.ressubCategory); 
    this.subcategories.push(this.ressubCategory);
       console.log(this.subcategories); 
     });

  }
  delete(id: Number): void {
    // use load ash here 
    //  let index=_.findIndex(this.countries, function(o) { return n == id; });
    console.log(id);
    this.subcategoryService.deletesubCategory(id)
    .subscribe(responce => {
        this.mesg = responce
        console.log(this.mesg);
    for (let i = 0; i < this.subcategories.length; i++) {
      if (id === this.subcategories[i]._id) {
        let index;
        index = i;
        this.subcategories.splice(index, 1)
        
        this.subcategories = this.subcategories;
   
        
      }
    }
     });
  }
  update(subcategory,id):void{
    for(let i =0;i<this.categories.length;i++){
      if(subcategory.parentCategory===this.categories[i].name){
        if(this.categories[i].categoryStatus==="Active")
        {
         this.isCreate = false;
         this.isUpdate = true;
         this.id = id
        }
        else{
          console.log("activate it category first ");

        }
      }
    }
  
    
  }
  updatesubCategory(updatedsubCountries:subCategory[]):void{
    this.subcategories=updatedsubCountries;
   
    console.log(this.subcategories)
      this.isCreate=true;
    this.isUpdate=false;
  }
  showSubCategoriesPagePage(condition: boolean) {
    this.getsubCategories();
    if (condition === true) {
      this.isAdd = false;
      this.isCreate = true;
    }
    else {
      this.isCreate = true;
      this.isUpdate = false;
    }

  }

}
