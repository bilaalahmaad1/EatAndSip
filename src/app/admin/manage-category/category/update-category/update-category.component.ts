import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {  Router } from '@angular/router';
import { Category,categoryService  } from '../../services/category.services';
import { Http, Response } from '@angular/http';
import {subCategory,subcategoryService} from '../../services/subCategory.service';
import {VendorDetail,vendorDetailService} from '../../../manage-vendor/services/vendorDetail.service';



 
@Component({
  selector: 'app-update-category',
  templateUrl: './update-category.component.html',
  styleUrls: ['./update-category.component.css']
})
export class UpdateCategoryComponent implements OnInit {
  tempcategory: string;
  vendors: VendorDetail[];
  @Output() isBack: EventEmitter<boolean> = new EventEmitter<boolean>();
  subCategories: subCategory[];
  category: Category;
  oldCategory:string;
  @Input() updateCategory: Category[];
  @Input() updateId: number;
  @Output() updatedCategory: EventEmitter<Category[]> = new EventEmitter<Category[]>();
  constructor(public vendorDetailService:vendorDetailService,private categoryService:categoryService,private subcategoryService:subcategoryService) {
    this.oldCategory="";
    this.vendors=[];
    this.tempcategory=""
   }

  ngOnInit() {
    this.subcategoryService.getsubCategoryList().subscribe(res => { this.subCategories = res });
    this.vendorDetailService.getVendorDetailList().subscribe(res=>{this.vendors=res})
          for (let i = 0; i < this.updateCategory.length; i++) {
      if (this.updateCategory[i]._id === this.updateId) {
        console.log("found object"+this.updateCategory[i] )
        this.category=this.updateCategory[i] ;
        this.oldCategory=this.category.name;
        this.tempcategory=this.category.name;
      }
  }


}
 update(category): void {

   
this.categoryService.updateCategory(this.updateId,category)
.subscribe((response:Response)=>{

  
  for(let a =0;a<this.vendors.length;a++){
  for(let b =0;b<this.vendors[a].categories.length;b++){
       if(this.vendors[a].categories[b]===this.tempcategory){
        this.vendors[a].categories[b]=category.name;
         this.vendorDetailService.updateVendorDetail(this.vendors[a]._id, this.vendors[a]).subscribe();
 
       }
     }
    }
     for(let j =0;j<this.subCategories.length;j++){
       if(this.subCategories[j].parentCategory===this.oldCategory){
        this.subCategories[j].parentCategory=category.name;
         this.subcategoryService.updatesubCategory(this.subCategories[j]._id, this.subCategories[j]).subscribe();

       }
     }

    for (let i = 0; i < this.updateCategory.length; i++) {
      if (this.updateCategory[i]._id === this.category._id) {
        this.updateCategory[i].name = category.name
        
        
      }
    }

});


  

    console.log("THIS OBJECT IS NOW UPDATED")
    console.log(this.updateCategory);
  
    this.updatedCategory.emit(this.updateCategory);
  }
  back(): void {
    // back to country with empty country
    // this.country.emit(this.newCountry);
    this.isBack.emit(false);

  }
  status(category,categoryStatus){
    if (category.categoryStatus === "Active") {
      
      for (let j = 0; j < this.subCategories.length; j++) {
        if (this.subCategories[j].parentCategory === category.name) {
          this.subCategories[j].sucCategoryStatus = "Active"
          // this.subCategories[j].parentCategory=category.name;
          //  console.log(this.cities[i]);
          this.subcategoryService.updatesubCategory(this.subCategories[j]._id, this.subCategories[j])
            .subscribe(res => {
              console.log(res);
            });
        }
      }
  
    }
    if (category.categoryStatus === "Deactive") {
      
      for (let j = 0; j < this.subCategories.length; j++) {
        if (this.subCategories[j].parentCategory === category.name) {
          this.subCategories[j].sucCategoryStatus = "Deactive"
          // this.subCategories[j].parentCategory=category.name;
          //  console.log(this.cities[i]);
          this.subcategoryService.updatesubCategory(this.subCategories[j]._id, this.subCategories[j])
            .subscribe(res => {
              console.log(res);
            });
        }
      }
  
    }

  }
}
