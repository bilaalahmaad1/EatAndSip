import { Component, OnInit } from '@angular/core';
import { Category, categoryService } from '../services/category.services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  categories: Category[];
  isUpdate: boolean;
  id:number;
  showCategories:boolean;
  constructor(private categoryservice: categoryService ,public router:Router) {
    this.categories = [];
    this.showCategories=true;
    this.isUpdate=false;
  }

  ngOnInit() {
    this.getCategories();

  }
  getCategories(): void {
    this.categoryservice.getCategoryList()
      .subscribe(responce => {
        if (responce.success === false) { this.router.navigate(['/login']);}
        this.categories = responce
        console.log(this.categories);


      });
  }
 update(id):void{
    console.log("HIT UPDATE BUTTON");
    this.showCategories=false;
    this.isUpdate=true;
    this.id=id

  }
  updateCategory(updatedCategories:Category[]):void{
    this.categories=updatedCategories;
    console.log("i am in updatecat function")
    console.log(this.categories)
    this.isUpdate=false;
      this.showCategories=true;
  }
  showCategoriesPage(condition: boolean) {
    this.getCategories();
    
    if(condition===false){
      this.showCategories=true;
      this.isUpdate = false;
    }

  }

}
