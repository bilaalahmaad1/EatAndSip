import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ManageCategoryComponent } from './manage-category/manage-category.component';
import { CategoryComponent } from './category/category.component';
import { SubCategoryComponent } from './sub-category/sub-category.component';
import { categoryService } from './services/category.services';
import { UpdateCategoryComponent } from './category/update-category/update-category.component';
// import { DropDownCategoriesComponent } from '../../shared/common/drop-down-categories/drop-down-categories.component';
import { AddSubCategoryComponent } from './sub-category/add-sub-category/add-sub-category.component';
import { UpdateSubCategoryComponent } from './sub-category/update-sub-category/update-sub-category.component';
import { subcategoryService } from './services/subCategory.service';
import {SharedModule} from '../../shared/shared.module';
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpModule,
    FormsModule,SharedModule
  ], providers: [categoryService, subcategoryService],
  exports: [ManageCategoryComponent],
  declarations: [ManageCategoryComponent, CategoryComponent, SubCategoryComponent, UpdateCategoryComponent, AddSubCategoryComponent, UpdateSubCategoryComponent]
})
export class ManageCategoryModule { }
