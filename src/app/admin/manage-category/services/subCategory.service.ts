import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '../../../shared/common/httpClient';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/Rx';


//model issue 
export class subCategory {
    public _id: number;
    public name: string;
    public parentCategory: string;
    public sucCategoryStatus:string;

    // constructor() { }
}
//model issue 

@Injectable()
export class subcategoryService {

    constructor(private http: Http, private HttpClient: HttpClient) {
    }

    getsubCategory(id: number) {
        return this.getsubCategoryList()
            .map(subCategories => subCategories.find(subCategory => subCategory.id === id))
            // .do(value => (console.log(value)))
            .catch(this.handlError);
    }

    getsubCategoryList() {


        let url = "subcategories";
        // console.log("sbvhk")
        return this.HttpClient
            .get(url)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
    postsubCategory(subcategory: subCategory) {
        let url = "subcategories"
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .post(url, subcategory)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
    updatesubCategory(id, subcategory: subCategory) {
        let url = "subcategories/" + id
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .put(url, subcategory)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
    deletesubCategory(id) {
        console.log(id)
        let url = "subcategories/" + id
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .delete(url)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }

    private handlError(error: Response) {
        let msg = `Error status code${error.status} at ${error.url}`;
        return Observable.throw(msg);
    }
}