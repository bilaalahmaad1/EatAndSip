import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import {HttpClient} from '../../../shared/common/httpClient';
import { Headers ,RequestOptions} from '@angular/http';


//model issue 
export class Category {
    public _id: number; 
    public name: string; 
    public categoryStatus:string;
    
    // constructor() { }
}
//model issue 

@Injectable()
export class categoryService {

    constructor(private http: Http,private HttpClient:HttpClient) {
    }

    getCategory(id: number) {
        return this.getCategoryList()
            .map(Categories => Categories.find(Category => Category.id === id))
            // .do(value => (console.log(value)))
            .catch(this.handlError);
    }

    getCategoryList() {

 
          let url = "categories";
            // console.log("sbvhk")
             return this.HttpClient
            .get(url)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
 postCategory(category: Category) {
        let url = "categories"
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .post(url, category)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
    updateCategory(id,category:Category){
          let url = "categories/"+id
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .put(url,category)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
  deleteCategory(id) {
      console.log(id)
        let url = "categories/"+id
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .delete(url)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }

    private handlError(error: Response) {
        let msg = `Error status code${error.status} at ${error.url}`;
        return Observable.throw(msg);
    }
}