import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageCuisinesComponent } from './manage-cuisines/manage-cuisines.component';
import { CuisinesComponent } from './cuisines/cuisines.component';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AddCuisineComponent } from './cuisines/add-cuisine/add-cuisine.component';
import { UpdateCuisineComponent } from './cuisines/update-cuisine/update-cuisine.component';
import{cuisineService} from './services/cuisine.service';


@NgModule({
  imports: [
    CommonModule,
     RouterModule,
    HttpModule,
    FormsModule
  ],
  providers:[cuisineService],
  declarations: [ ManageCuisinesComponent, CuisinesComponent, AddCuisineComponent, UpdateCuisineComponent],
  exports:[ManageCuisinesComponent]
})
export class ManageCuisinesModule { }
