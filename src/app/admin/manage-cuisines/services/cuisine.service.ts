import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import {HttpClient} from '../../../shared/common/httpClient';
import { Headers ,RequestOptions} from '@angular/http';


//model issue 
export class Cuisine {
    public _id: number; 
    public name: string; 
    public sortOrder: string;
    public cusineStatus:string;
    // constructor() { }
}
//model issue 

@Injectable()
export class cuisineService {

    constructor(private http: Http,private HttpClient:HttpClient) {
    }

    getCuisine(id: number) {
        return this.getCuisineList()
            .map(Cuisines => Cuisines.find(Cuisine => Cuisine.id === id))
            // .do(value => (console.log(value)))
            .catch(this.handlError);
    }

    getCuisineList() {

 
          let url = "cuisines";
            // console.log("sbvhk")
             return this.HttpClient
            .get(url)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
 postCuisine(cuisine: Cuisine) {
        let url = "cuisines"
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .post(url, cuisine)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
    updateCuisine(id,cuisine:Cuisine){
          let url = "cuisines/"+id
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .put(url,cuisine)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
  deleteCuisine(id) {
      console.log(id)
        let url = "cuisines/"+id
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .delete(url)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }

    private handlError(error: Response) {
        let msg = `Error status code${error.status} at ${error.url}`;
        return Observable.throw(msg);
    }
}