import { Component, OnInit } from '@angular/core';
import {cuisineService,Cuisine } from '../services/cuisine.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cuisines',
  templateUrl: './cuisines.component.html',
  styleUrls: ['./cuisines.component.css']
})
export class CuisinesComponent implements OnInit {
 cuisines: Cuisine[];
  isCreate: boolean;
  isAdd: boolean;
  isUpdate: boolean;
  id:number;
  resCuisine:Cuisine;
 mesg:string;
  constructor(public cuisineservice: cuisineService,public router:Router) {
      this.cuisines = [];
    this.isCreate = true;
    this.isAdd = false;
    this.isUpdate = false;
    this.mesg="";
   }


  ngOnInit() {
    this.getCuisines();
  }
  getCuisines(): void {
    this.cuisineservice.getCuisineList()
      .subscribe(responce => {
        if (responce.success === false) { this.router.navigate(['/login']);}
        this.cuisines = responce
        console.log("i am in manage cuisine get cuisines from service");
        console.log(this.cuisines);
        // console.log("jjjjjjjjjjjjjj")
        
      });
  }
  createCuisine(): void {
    console.log("create button hits");
    this.isAdd = true;
    this.isCreate = false;

  }
  addCuisine(cuisine: Cuisine) {
    console.log("I am in addcuisine function new cuisine comes from add component");
    console.log(cuisine);
    this.isAdd = false;
    this.isCreate = true;
    
    //add post method of service to send data in data base 
    console.log("we are adding countru to servise to db");
    this.cuisineservice.postCuisine(cuisine)
    .subscribe(responce => {
        this.resCuisine = responce
      //  console.log("log1")
      console.log("after adding new cuisine to db we get this response");
        console.log(this.resCuisine)
        //  console.log("log2")
    this.cuisines.push(this.resCuisine);
   
      });
      


  }
  delete(id: Number): void {
    // use load ash here 
   
    console.log(id);
    this.cuisineservice.deleteCuisine(id)
    .subscribe(responce => {
        this.mesg = responce
        console.log(this.mesg);
         for (let i = 0; i < this.cuisines.length; i++) {
      if (id === this.cuisines[i]._id) {
        let index;
        index = i;
       console.log(this.cuisines[i]._id)
        // console.log(index)
        this.cuisines.splice(index, 1)
 
        this.cuisines = this.cuisines;
        //  this.newemployeedata=this.delEmployee
      
        // console.log(id)
      }
    }
      
    
    
      });
   
  }
  update(id):void{
    console.log("HIT UPDATE BUTTON");
    this.isCreate=false;
    this.isUpdate=true;
    this.id=id

  }
  updateCuisine(updatedCuisines:Cuisine[]):void{
    this.cuisines=updatedCuisines;
    console.log("i am in updateCuisine function")
    console.log(this.cuisines)
      this.isCreate=true;
    this.isUpdate=false;
  }
  showcuisinePage(condition: boolean) {
    this.getCuisines();
    if (condition === true) {
      this.isAdd = false;
      this.isCreate = true;
    }
    else {
      this.isCreate = true;
      this.isUpdate = false;
    }

  }
}

