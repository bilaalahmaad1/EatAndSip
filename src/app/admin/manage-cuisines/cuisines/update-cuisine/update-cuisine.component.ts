import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Cuisine,cuisineService  } from '../../services/cuisine.service';
import { Http, Response } from '@angular/http';
import {VendorDetail,vendorDetailService} from '../../../manage-vendor/services/vendorDetail.service';




@Component({
  selector: 'app-update-cuisine',
  templateUrl: './update-cuisine.component.html',
  styleUrls: ['./update-cuisine.component.css']
})
export class UpdateCuisineComponent implements OnInit {
  tempcuisine: string;
  isStatus: any;
  cuisine: Cuisine;
  vendors: VendorDetail[];
  @Input() updateCuisine: Cuisine[];
  @Input() updateId: number;
  @Output() updatedCuisine: EventEmitter<Cuisine[]> = new EventEmitter<Cuisine[]>();
  @Output() isBack: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor(public vendorDetailService:vendorDetailService,public cuisineService: cuisineService) {
    this.vendors=[];
    this.tempcuisine=""
   }

  ngOnInit() {
    this.vendorDetailService.getVendorDetailList().subscribe(res=>{this.vendors=res});
      console.log("THIS ID WANT TO UPDTAE "+this.updateId)

          for (let i = 0; i < this.updateCuisine.length; i++) {
      if (this.updateCuisine[i]._id === this.updateId) {
        console.log("found object"+this.updateCuisine[i] )
        this.cuisine=this.updateCuisine[i] ;
        this.tempcuisine=this.cuisine.name;
        
      }
     
    }
    console.log("THIS OBJECT IS GOING TO UPDATE")
 console.log(this.cuisine)
  }
  update(cuisine): void {
    console.log("hi i awan to update "+cuisine)
      console.log(cuisine)
      // cuisine.cusineStatus = this.isStatus;
      console.log(this.isStatus)
this.cuisineService.updateCuisine(this.updateId,cuisine)
.subscribe((response:Response)=>{


  for(let a =0;a<this.vendors.length;a++){
    for(let b =0;b<this.vendors[a].cusines.length;b++){
         if(this.vendors[a].cusines[b]===this.tempcuisine){
          this.vendors[a].cusines[b]=cuisine.name;
           this.vendorDetailService.updateVendorDetail(this.vendors[a]._id, this.vendors[a]).subscribe();
   
         }
       }
      }
  
    for (let i = 0; i < this.updateCuisine.length; i++) {
      if (this.updateCuisine[i]._id === this.cuisine._id) {
        this.updateCuisine[i].name = cuisine.name
        this.updateCuisine[i].sortOrder = cuisine.sortOrder
        
      }
    }

});


  

    console.log("THIS OBJECT IS NOW UPDATED")
    console.log(this.updateCuisine);
  
    this.updatedCuisine.emit(this.updateCuisine);
  }
  back(): void {
    // back to country with empty country
    // this.country.emit(this.newCountry);
    this.isBack.emit(false);

  }
  status(isStatus) {
    this.isStatus = isStatus;
  }
}



