import { Component,OnInit, Output, EventEmitter  } from '@angular/core';
import { Cuisine } from '../../services/cuisine.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-cuisine',
  templateUrl: './add-cuisine.component.html',
  styleUrls: ['./add-cuisine.component.css']
})
export class AddCuisineComponent implements OnInit {
  isStatus: string;
  @Output() cuisine: EventEmitter<Cuisine> = new EventEmitter<Cuisine>();
 @Output () isBack: EventEmitter<boolean> = new EventEmitter<boolean>();
  newCuisine: Cuisine;
  constructor() {
      this.newCuisine= new Cuisine();
      this.isStatus="";
   }

  ngOnInit() {
  }
 save(): void {
  this.newCuisine.cusineStatus=this.isStatus;
    this.cuisine.emit(this.newCuisine);
  }
  back(): void {
    
    this.isBack.emit(true);

  }
  status(isStatus){
    this.isStatus=isStatus;
  }
}
