import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { HttpClient } from '../../../shared/common/httpClient';
import { Headers, RequestOptions } from '@angular/http';


//model issue 
export class IngredientType {
    public _id: number;
    public name: string;
    public ingredientTypeStatus:string;
   
    // constructor() { }
}
//model issue 

@Injectable()
export class ingredienttypeService {

    constructor(private http: Http, private HttpClient: HttpClient) {
    }

    getIngredientType(id: number) {
        return this.getIngredientTypeList()
            .map(IngredientTypes => IngredientTypes.find(IngredientType => IngredientType.id === id))
            // .do(value => (console.log(value)))
            .catch(this.handlError);
    }

    getIngredientTypeList() {

        // return this.http
        //     .get('assets/city.json')
        //     .map((response: Response) => <City[]>response.json().City)
        //     // .do(value => console.log(value))
        //     .catch(this.handlError);
        let url = "ingredienttypes";
        // console.log("sbvhk")
        return this.HttpClient
            .get(url)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
    postIngredientType(ingredienttype: IngredientType) {
        let url = "ingredienttypes"
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .post(url, ingredienttype)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
    updateIngredientType(id, ingredienttype: IngredientType) {
        let url = "ingredienttypes/" + id
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .put(url, ingredienttype)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
    deleteIngredientType(id) {
        console.log(id)
        let url = "ingredienttypes/" + id
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .delete(url)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }

    private handlError(error: Response) {
        let msg = `Error status code${error.status} at ${error.url}`;
        return Observable.throw(msg);
    }
}