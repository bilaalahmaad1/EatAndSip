import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { HttpClient } from '../../../shared/common/httpClient'
import { Headers, RequestOptions } from '@angular/http';
   


//model issue 
export class IngredientList {
    public _id: number;
    public names: string[];
    public prices:string[];
    public IngredientType: string;
    public IngredientListStatus:string;
    public ingredienttypename:string
    // constructor() { }
}
//model issue 

@Injectable()
export class ingredientListService {

    constructor(private http: Http, private HttpClient: HttpClient) {
    }

    getIngredientList(id: number) {
        return this.getIngredientListList()
            .map(IngredientLists => IngredientLists.find(IngredientList => IngredientList.id === id))
            // .do(value => (console.log(value)))
            .catch(this.handlError);
    }

    getIngredientListList() {

        // return this.http
        //     .get('assets/city.json')
        //     .map((response: Response) => <City[]>response.json().City)
        //     // .do(value => console.log(value))
        //     .catch(this.handlError);
        let url = "ingredientlists";
        // console.log("sbvhk")
        return this.HttpClient
            .get(url)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
    postIngredientList(ingredientlist: IngredientList) {
        let url = "ingredientlists"
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .post(url, ingredientlist)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
    updateIngredientList(id, ingredientlist: IngredientList) {
        let url = "ingredientlists/" + id
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .put(url, ingredientlist)
            .map((response: Response) => {
                let data = response.json();
                return data;
            });
    }
    deleteIngredientList(id) {
        console.log(id)
        let url = "ingredientlists/"+ id
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.HttpClient
            .delete(url)
            .map((response: Response) => {
                // console.log(response);

                let data = response.json();
                // console.log(data);

                return data;

            });
    }

    private handlError(error: Response) {
        let msg = `Error status code${error.status} at ${error.url}`;
        return Observable.throw(msg);
    }
}