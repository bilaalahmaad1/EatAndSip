import { Component, OnInit } from '@angular/core';
import { IngredientList, ingredientListService } from '../services/ingredientlist.service';
import { IngredientType, ingredienttypeService } from '../services/ingredienttype.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ingredient-type',
  templateUrl: './ingredient-type.component.html',
  styleUrls: ['./ingredient-type.component.css']
})
export class IngredientTypeComponent implements OnInit {
  id: number;
  ingredientList: IngredientList[];
  mesg: any;
  isCreate: Boolean
  isAdd: Boolean
  isUpdate: Boolean
  ingredientTypes: IngredientType[];
  resIngredientType: IngredientType;
  constructor(public ingredientListService: ingredientListService, public ingredienttypeService: ingredienttypeService, public router:Router) {
    this.isAdd = false;
    this.isCreate = true;
    this.isUpdate = false
    this.resIngredientType = new IngredientType();
    this.ingredientTypes = [];
  }

  ngOnInit() {
    this.getIngredientTypes();
    this.ingredientListService.getIngredientListList().subscribe(res => { this.ingredientList = res; console.log(this.ingredientList) });
    // this.countryService.getCountryList().subscribe(res=>{this.countries=res});
  }
  getIngredientTypes(): void {
    this.ingredienttypeService.getIngredientTypeList()
      .subscribe(responce => {
        if (responce.success === false) { this.router.navigate(['/login']);}
        this.ingredientTypes = responce
        console.log(this.ingredientTypes)
      });
  }
  createIngredientType() {
    this.isAdd = true;
    this.isCreate = false;
  }
  addIngredientType(ingredientType: IngredientType) {

    this.isAdd = false;
    this.isCreate = true;

    this.ingredienttypeService.postIngredientType(ingredientType)
      .subscribe(responce => {
        this.resIngredientType = responce
        this.ingredientTypes.push(this.resIngredientType);
      });

  }
  addIngredientList(ingredientList) {
    this.ingredientListService.postIngredientList(ingredientList)
      .subscribe(responce => {
        console.log(responce);

      });
  }
  delete(id: Number,index): void {

    // console.log(id);
    // this.ingredienttypeService.deleteIngredientType(id)
    //   .subscribe(responce => {
    //     this.mesg = responce
    //     console.log(this.mesg);
    //     this.ingredientTypes.splice(index, 1)
        
    //      this.ingredientTypes = this.ingredientTypes;
        
          for (let k = 0; k < this.ingredientList.length; k++) {
            if ( this.ingredientTypes[index].name===this.ingredientList[k].IngredientType) {
              // console.log(this.ingredientList[k]);
              this.ingredientListService.deleteIngredientList(this.ingredientList[k]._id)
                .subscribe(res => console.log(res));
            }
          }
this.ingredienttypeService.deleteIngredientType(id)
      .subscribe(responce => {
        this.mesg = responce
        console.log(this.mesg);
        this.ingredientTypes.splice(index, 1)
        
         this.ingredientTypes = this.ingredientTypes;
        
       
      });

  }

  update(id) {
    this.isCreate = false;
    this.isUpdate = true;
    this.id = id
  }
  updateIngredientType(IngredientType){
    this.ingredientTypes = IngredientType;
    
    this.isCreate = true;
    this.isUpdate = false;

  }
  showingredientPage(condition: boolean) {
    this.getIngredientTypes();
    if (condition === true) {
      this.isAdd = false;
      this.isCreate = true;
    }
    else {
      this.isCreate = true;
      this.isUpdate = false;
    }

  }
}
