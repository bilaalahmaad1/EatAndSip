import { Component, OnInit, ViewChild, ElementRef ,EventEmitter,Output} from '@angular/core';
import {IngredientList,ingredientListService} from '../../services/ingredientlist.service';
import {IngredientType,ingredienttypeService} from '../../services/ingredienttype.service';
 class namesoflist{
   public name:string;
   public price:string;
 }
@Component({
  selector: 'app-add-ingredient',
  templateUrl: './add-ingredient.component.html',
  styleUrls: ['./add-ingredient.component.css']
})
export class AddIngredientComponent implements OnInit {
  @Output () isBack: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output () ingredientType:EventEmitter<IngredientType> =new EventEmitter<IngredientType>();
  @Output () ingredientList:EventEmitter<IngredientList> =new EventEmitter<IngredientList>();
  isStatus: string;
  showListOfIngredients: boolean;
  // listCounter: string[];
  newIngredientType:IngredientType;
  newIngredientList:IngredientList;
  
  ingredientlists:namesoflist[]=[];
  ingredientlist:namesoflist;
  
  
 
  @ViewChild('myRadioyes') public radioyes: ElementRef;
  @ViewChild('myRadiono') public radiono: ElementRef;
  constructor() {
    
    this.newIngredientList= new IngredientList();
    this.newIngredientList.names=[];
    this.newIngredientList.prices=[];
    this.newIngredientType=new IngredientType ();
    
  }

  ngOnInit() {
    
   this.ingredientlist= new namesoflist();
   this.ingredientlist.name="";
    this.ingredientlists.push( this.ingredientlist);
    this.radiono.nativeElement.checked = true;
  }
  save(){
   this.newIngredientType.ingredientTypeStatus=this.isStatus;
   
  
 
  this.ingredientType.emit(this.newIngredientType);

  if(this.ingredientlists.length>0&& this.ingredientlist.name!==""){
    for(let i=0;i<this.ingredientlists.length;i++){
      this.newIngredientList.names[i]=this.ingredientlists[i].name;
      this.newIngredientList.prices[i]=this.ingredientlists[i].price;
    }
    this.newIngredientList.IngredientListStatus=this.newIngredientType.ingredientTypeStatus
    this.newIngredientList.IngredientType=this.newIngredientType.name
    this.ingredientList.emit(this.newIngredientList);
  }
  
  }
 
  showList(value: string) {


    if (value === "yes") {
      this.showListOfIngredients = true;
      
     
      if (this.ingredientlists.length < 1) {
      
        this.ingredientlist= new namesoflist();
        this.ingredientlist.name="";
         this.ingredientlists.push( this.ingredientlist);
      }
     


    } else {
      this.showListOfIngredients = false;
      this.ingredientlists.splice(0,this.ingredientlists.length);


    }

  }
  pluslsit(i) {
   
    this.ingredientlist= new namesoflist();
    this.ingredientlist.name="";
     this.ingredientlists.push( this.ingredientlist);
     console.log(this.ingredientlists)
    

  }
  minuslist(i) {
   
    this.ingredientlists.splice(i, 1);
    console.log(this.ingredientlists)


    if (this.ingredientlists.length < 1) {
      this.radioyes.nativeElement.checked = false
      this.radiono.nativeElement.checked = true;
    }


  }
  back(): void {
    
    this.isBack.emit(true);

  }
  status(isStatus){
    this.isStatus=isStatus;
  }

}
