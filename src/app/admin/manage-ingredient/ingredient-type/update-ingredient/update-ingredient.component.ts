import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import {IngredientList,ingredientListService} from '../../services/ingredientlist.service';
import {IngredientType,ingredienttypeService} from '../../services/ingredienttype.service';
class namesoflist{
  public name:string;
  public price:string;
}
@Component({
  selector: 'app-update-ingredient',
  templateUrl: './update-ingredient.component.html',
  styleUrls: ['./update-ingredient.component.css']
})
export class UpdateIngredientComponent implements OnInit {
  nolistofingredient: boolean;
  newIngredientList: IngredientList;
  ingredientTyptemp: string;
  ingredientType: IngredientType;
  ingredientTypes: IngredientType[];
  ingredientLists: IngredientList[];
  showListOfIngredients: boolean;
  ingredientlists:namesoflist[]=[];
  ingredientlist:namesoflist;
  @Input() updateId: number;
  @Output() isBack: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() updateIngredientType: IngredientType[];
  @Output() updatedIngredientType: EventEmitter<IngredientType[]> = new EventEmitter<IngredientType[]>();
  isStatus: any;
  @ViewChild('myRadioyes') public radioyes: ElementRef;
  @ViewChild('myRadiono') public radiono: ElementRef;
  constructor(public ingredienttypeService: ingredienttypeService , public ingredientListService:ingredientListService) {
    this.ingredientTyptemp=""
    this.ingredientType=new IngredientType();
    this.ingredientTypes=[];
    this.ingredientlists=[];
    this.ingredientLists=[];
    this.newIngredientList= new IngredientList();
    this.newIngredientList.names=[];
    this.newIngredientList.prices=[];
    this.nolistofingredient=true;
   

   }

  ngOnInit() {
   
    this.ingredientListService.getIngredientListList()
    .subscribe(res=>{this.ingredientLists=res;
      this.radiono.nativeElement.checked = true;
      this.nolistofingredient=true
      for(let k =0;k<this.ingredientLists.length;k++){
        if(this.ingredientLists[k].IngredientType=== this.ingredientType.name){
         
            this.radioyes.nativeElement.checked = true
            this.showListOfIngredients=true;
            this.radiono.nativeElement.checked = false;
            this.nolistofingredient=false
           
           for(let l =0;l<this.ingredientLists[k].names.length;l++){
             this.ingredientlist= new namesoflist();
             this.ingredientlist.name=this.ingredientLists[k].names[l];
             this.ingredientlist.price=this.ingredientLists[k].prices[l];
             this.ingredientlists.push( this.ingredientlist);
           }
          
        }
       
      }
    
    
    });
    this.ingredienttypeService.getIngredientTypeList()
    .subscribe(res=>{this.ingredientTypes=res});
    for (let i = 0; i < this.updateIngredientType.length; i++) {
      if (this.updateIngredientType[i]._id === this.updateId) {
        console.log("found object" + this.updateIngredientType[i])
        this.ingredientType = this.updateIngredientType[i];
        this.ingredientTyptemp=this.ingredientType.name;

      }

    }

    

  }
  showList(value: string) {
    
    
        if (value === "yes") {
          this.showListOfIngredients = true;
          
         
          if (this.ingredientlists.length < 1) {
          
            this.ingredientlist= new namesoflist();
            this.ingredientlist.name="";
             this.ingredientlists.push( this.ingredientlist);
          }
         
    
    
        } else {
          this.showListOfIngredients = false;
          this.ingredientlists.splice(0,this.ingredientlists.length);
          this.newIngredientList.names.splice(0,  this.newIngredientList.names.length);
          this.newIngredientList.prices.splice(0,  this.newIngredientList.prices.length);
    
    
        }
    
      }
      pluslsit(i,ingredientlists) {
        
         this.ingredientlist= new namesoflist();
         this.ingredientlist.name="";
          this.ingredientlists.push( this.ingredientlist);
          console.log(this.ingredientlists)
          for(let i=0;i<this.ingredientlists.length;i++){
            this.newIngredientList.names[i]=ingredientlists[i].name;
            this.newIngredientList.prices[i]=ingredientlists[i].price;
          }
         
         
     
       }
       minuslist(i,ingredientlists) {
        
         this.ingredientlists.splice(i, 1);
     
          this.newIngredientList.names.splice(i, 1);
          this.newIngredientList.prices.splice(i, 1);
        
         console.log(this.ingredientlists )
     
     
         if (this.ingredientlists.length < 1) {
           this.radioyes.nativeElement.checked = false
           this.radiono.nativeElement.checked = true;
         }
     
     
       }
       update(ingredientType,ingredientlists){
         console.log(ingredientType);
         console.log(ingredientlists);

        if(this.ingredientlists.length>0&& this.ingredientlist.name!==""){
          for(let i=0;i<this.ingredientlists.length;i++){
            this.newIngredientList.names[i]=ingredientlists[i].name;
            this.newIngredientList.prices[i]=ingredientlists[i].price;
          }
          this.newIngredientList.IngredientListStatus=ingredientType.ingredientTypeStatus
          this.newIngredientList.IngredientType=ingredientType.name
         
        }
       this.ingredienttypeService.updateIngredientType(this.updateId,ingredientType)
       .subscribe(res=>{

        for(let k =0;k<this.ingredientLists.length;k++){
         
          if(this.ingredientLists[k].IngredientType===this.ingredientTyptemp){
            if(this.nolistofingredient===false&& this.radioyes.nativeElement.checked===true){
            // this.nolistofingredient=false
            // if(this.ingredientlists.length>1){
            this.ingredientListService.updateIngredientList(this.ingredientLists[k]._id, this.newIngredientList).subscribe(
              res=>{console.log(res)}
            );
          }
          if(this.nolistofingredient===false&& this.radioyes.nativeElement.checked===false){
            this.ingredientListService.deleteIngredientList(this.ingredientLists[k]._id).subscribe(
              res=>{console.log(res)}
            );

          }
          // }
          
         
            //
          
        }
      }
        if(this.nolistofingredient===true&&this.radioyes.nativeElement.checked===true){
          this.ingredientListService.postIngredientList(this.newIngredientList).subscribe();

        }



        for (let i = 0; i < this.updateIngredientType.length; i++) {
          if (this.updateIngredientType[i]._id === this.updateId) {
            this.updateIngredientType[i].name = this.ingredientType.name
            this.updateIngredientType[i].ingredientTypeStatus = this.ingredientType.ingredientTypeStatus;
          }
        }
       });
       console.log(this.updateIngredientType);
         this.updatedIngredientType.emit(this.updateIngredientType);
       }
       
  back(): void {
    
    this.isBack.emit(false);

  }

}
