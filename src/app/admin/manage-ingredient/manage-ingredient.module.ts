import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageIngredientComponent } from './manage-ingredient/manage-ingredient.component';
import { IngredientTypeComponent } from './ingredient-type/ingredient-type.component';
import { IngredientListComponent } from './ingredient-list/ingredient-list.component';
import { UpdateIngredientComponent } from './ingredient-type/update-ingredient/update-ingredient.component';
import { AddIngredientComponent } from './ingredient-type/add-ingredient/add-ingredient.component';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import {ingredientListService} from './services/ingredientlist.service'; 
import {ingredienttypeService} from './services/ingredienttype.service';



@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    RouterModule,
    BrowserModule,
    FormsModule
  ],
  declarations: [ManageIngredientComponent, IngredientTypeComponent, IngredientListComponent, UpdateIngredientComponent, AddIngredientComponent],
  exports:[ManageIngredientComponent],
  providers:[ingredienttypeService,ingredientListService,ManageIngredientComponent]
})
export class ManageIngredientModule { }
