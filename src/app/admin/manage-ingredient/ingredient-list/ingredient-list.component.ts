import { Component, OnInit } from '@angular/core';
import { IngredientList,ingredientListService} from '../services/ingredientlist.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ingredient-list',
  templateUrl: './ingredient-list.component.html',
  styleUrls: ['./ingredient-list.component.css']
})
export class IngredientListComponent implements OnInit {
  ingredientLists: IngredientList[];
  listnamestring:string[];
  listpricestring:string[];
  isCreate:boolean=true;
  

  constructor(public ingredientListService:ingredientListService,public router:Router) { 
 this.listnamestring=[]
    this.ingredientLists=[];
    this.listpricestring=[];
  }

  ngOnInit() {
    this.ingredientListService.getIngredientListList().subscribe(res => { 
      if (res.success === false) { this.router.navigate(['/login']);}
      this.ingredientLists = res;
    this.makeListnamestring();});
    

  }
  makeListnamestring(){
    for(let i=0;i<this.ingredientLists.length;i++){
      this.listnamestring[i]=this.ingredientLists[i].names.join("/");
      // this.listpricestring[i]=this.ingredientList[i].prices.join("/");

      console.log(this.listnamestring[i]);
    }
    for(let i=0;i<this.ingredientLists.length;i++){
     this.ingredientLists[i].ingredienttypename=this.listnamestring[i]
      

    }
    
  }
  delete(id){
    this.ingredientListService.deleteIngredientList(id).subscribe(res=>{console.log(res)});

  }


}
