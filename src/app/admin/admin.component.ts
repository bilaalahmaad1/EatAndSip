import { Component, OnInit } from '@angular/core';
import { adminService } from './service/admin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {


  constructor(public adminService: adminService, private router: Router) { }

  ngOnInit() {
    // this.router.navigate(['/admin']);
    // console.log(this.router.url)
    // if (this.router.url === "/admin") {
      this.adminService.getAdmin()
        .subscribe(res => {
          // console.log( "admin response")
          // console.log( res)

          if (res.success === true) {
            // console.log(res.success)
             
            this.router.navigate(['/admin']);





          }
          if (res.success === false) { this.router.navigate(['/login']);}


        });
    }
  // }

}
