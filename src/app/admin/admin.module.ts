import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageAddressModule } from './manage-address/manage-address.module';
import { ManageCategoryModule } from './manage-category/manage-category.module';
import { ManageCuisinesModule } from './manage-cuisines/manage-cuisines.module';
// import { ManageIngredientsModule } from './manage-ingredients/manage-ingredients.module';
import { ManageVendorModule } from './manage-vendor/manage-vendor.module';

import { ManageAddressComponent } from './manage-address/manage-address/manage-address.component';
import { ManageCategoryComponent } from './manage-category/manage-category/manage-category.component';
import { ManageCuisinesComponent } from './manage-cuisines/manage-cuisines/manage-cuisines.component';
import { ManageIngredientComponent } from './manage-ingredient/manage-ingredient/manage-ingredient.component';
import { ManageVendorComponent } from './manage-vendor/manage-vendor/manage-vendor.component';
import { AdminComponent } from './admin.component';
import { NavigationComponent } from './navigation/navigation.component';
import { RouterModule } from '@angular/router';
import { ManageIngredientModule } from './manage-ingredient/manage-ingredient.module';
import { adminService } from './service/admin.service';
import { adminsService } from './service/admins.service';
// import {ManageAddressComponent} from './manage-address/manage-address/manage-address.component';
import { SharedModule } from '../shared/shared.module';
import { ProfileModule } from '../profile/profile.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,

    ManageAddressModule,
    ManageCategoryModule,
    ManageCuisinesModule,
    ManageIngredientModule,

    ManageVendorModule,
    RouterModule,
    ProfileModule,
    FormsModule,
  ], providers: [adminService, adminsService],
  exports: [
    AdminComponent,

  ],
  declarations: [AdminComponent, NavigationComponent]
})
export class AdminModule { }
