import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { subCategory, subcategoryService } from '../../../admin/manage-category/services/subCategory.service';
import { Category, categoryService } from '../../../admin/manage-category/services/category.services';


@Component({
  selector: 'app-drop-down-sub-categories',
  templateUrl: './drop-down-sub-categories.component.html',
  styleUrls: ['./drop-down-sub-categories.component.css']
})
export class DropDownSubCategoriesComponent implements OnInit {
  @Output() subCategoryName: EventEmitter<subCategory> = new EventEmitter<subCategory>();
  @Input() category :string;
  @Input() populatesubcategories:boolean;
  tempsubCategory: any;
  selectedSubCategory: any;
  subCategories: subCategory[] = [];
  constructor(public subcategoryService: subcategoryService,
    public categoryService: categoryService) { }

  ngOnInit() {
console.log("i am in sub category component");

   
  }
  getSelectedSubCategory(selectedSubCategory) {
    console.log("hihihi");
    this.subCategoryName.emit(selectedSubCategory)

  }
  hah(){
    this.subCategories.splice(0,this.subCategories.length)
    console.log("haha");
    this.subcategoryService.getsubCategoryList()
    .subscribe(response => {
      this.tempsubCategory = response;

      for (let i = 0; i < this.tempsubCategory.length; i++) {
        if (this.tempsubCategory[i].sucCategoryStatus === "Active") {
          if(this.tempsubCategory[i].parentCategory===this.category){
            this.subCategories.push(this.tempsubCategory[i]);
          }
          
        }
      }
    });

  }

}
