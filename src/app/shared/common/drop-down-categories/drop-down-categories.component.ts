import { Component, OnInit ,EventEmitter,Output} from '@angular/core';
import {Category,categoryService} from '../../../admin/manage-category/services/category.services'; 

@Component({
  selector: 'app-drop-down-categories',
  templateUrl: './drop-down-categories.component.html',
  styleUrls: ['./drop-down-categories.component.css']
})
export class DropDownCategoriesComponent implements OnInit {
  tempcategories: any;
  categories: Category[]=[];
  selectedCategory:any;
    @Output() categoryName: EventEmitter<Category> = new EventEmitter<Category>();
  constructor(private categoryService:categoryService) { }

  ngOnInit() {
   
    
    this.categoryService.getCategoryList()
    .subscribe(response=>{ this.tempcategories=response; 
        for(let i =0;i<this.tempcategories.length;i++){
          if(this.tempcategories[i].categoryStatus==="Active"){
            this.categories.push(this.tempcategories[i]);
          }
        }

     
    });
     
   
  }
   getSelectedCategroy(selectedCategoryname){
  
  
  this.categoryName.emit(selectedCategoryname);
}

}
