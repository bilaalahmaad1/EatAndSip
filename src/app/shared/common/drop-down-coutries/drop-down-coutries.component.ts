import { Component, OnInit ,EventEmitter,Output} from '@angular/core';
import {countryService,Country}from '../../../admin/manage-address/services/country.service';
// import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-drop-down-coutries',
  templateUrl: './drop-down-coutries.component.html',
  styleUrls: ['./drop-down-coutries.component.css']
})
export class DropDownCoutriesComponent implements OnInit {
  // myform: FormGroup;
  tempcountries: Country[];
  countries: Country[]=[];
  selectedCountry:String;
   @Output() countryName: EventEmitter<Country> = new EventEmitter<Country>();
  constructor(private countryService:countryService) {
    this.selectedCountry='Enter Country';
    
   }

  ngOnInit() {
    console.log('countries')
    this.countryService.getCountryList()
    .subscribe(response=>{ this.tempcountries=response; 
        for(let i =0;i<this.tempcountries.length;i++){
          if(this.tempcountries[i].countryStatus==="Active"){
            this.countries.push(this.tempcountries[i]);
          }
        }

     
    });
    //  console.log('selectedcountries')
    //   console.log(this.selectedCountry)
    
  }
getSelectedCountry(selectedCountryname){
  
  // console.log(selectedCountryname)
  this.countryName.emit(selectedCountryname);
}
}
