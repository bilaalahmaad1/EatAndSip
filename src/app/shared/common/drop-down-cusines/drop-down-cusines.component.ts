import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import {Cuisine,cuisineService} from '../../../admin/manage-cuisines/services/cuisine.service';


@Component({
  selector: 'app-drop-down-cusines',
  templateUrl: './drop-down-cusines.component.html',
  styleUrls: ['./drop-down-cusines.component.css']
})
export class DropDownCusinesComponent implements OnInit {
  tempcusines:any;
  selectedCusine:any;
  cusines:Cuisine[]=[];
  @Output() cusineName: EventEmitter<Cuisine> = new EventEmitter<Cuisine>();
  constructor(public cuisineService:cuisineService) { }

  ngOnInit() {
    this.cuisineService.getCuisineList()
    .subscribe(response=>{ this.tempcusines=response; 
        for(let i =0;i<this.tempcusines.length;i++){
          if(this.tempcusines[i].cusineStatus==="Active"){
            this.cusines.push(this.tempcusines[i]);
          }
        }
  });
  }
  getSelectedCusine(selectedCusine){
    

    this.cusineName.emit(selectedCusine);

  }
}
