import { Injectable, Inject } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Config } from './config';
@Injectable()
export class HttpClient {
    private headervalue
     private _token: string;
    private apiUrl: string;

    constructor(private http: Http) {
    this.apiUrl = Config.END_POINT_URL;

  }
  getApiUrl(url: string) {
    return this.apiUrl + url;
  }
  createAuthorizationHeader(headers: Headers) {
    headers.append('Content-Type', 'application/json');
     headers.append('Authorization', this._token);
    // console.log("token is set in auth header");
    }
 get(url: string, headerValues?: any[]) {
       this._token = JSON.parse(localStorage.getItem("login")); 
    
    // console.log(this._token);
    // console.log("getting token from local storage")
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
      //  console.log(this.getApiUrl(url))
    headers.set('Accept', 'application/json');
    return this.http.get(this.getApiUrl(url), {
      headers: headers
    });
  }
  post(url: string, data: any, headerValues?: any[]) {
    this._token = JSON.parse(localStorage.getItem("login")); 
    //  console.log("token from local db")
   // console.log(this._token)
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    // for (let h of headerValues) {
    //   headers.append(h.key, h.value);
    // }
    headers.set('Accept', 'application/json');
    // console.log("httpclient service")
    // console.log(this.getApiUrl(url))
    // console.log(data)
    return this.http.post(this.getApiUrl(url), data, {
      headers: headers
    });
  }

  put(url: string, data: any, headerValues?: any[]) {
    this._token = JSON.parse(localStorage.getItem("login")); 
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    headers.set('Accept', 'application/json');
    return this.http.put(this.getApiUrl(url), data, {
      headers: headers
    });
  }

  delete(url: string, headerValues?: any[]) {
    this._token = JSON.parse(localStorage.getItem("login")); 
    // this._token = localStorage.getItem('login');
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    headers.set('Accept', 'application/json');
    // console.log(this.getApiUrl(url))
    // console.log(headers);

    return this.http.delete(this.getApiUrl(url), {
      headers: headers
    });
  }

}