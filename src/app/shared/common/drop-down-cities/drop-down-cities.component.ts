import { Component, OnInit ,EventEmitter,Output} from '@angular/core';
import {cityService,City}from '../../../admin/manage-address/services/city.service';

@Component({
  selector: 'app-drop-down-cities',
  templateUrl: './drop-down-cities.component.html',
  styleUrls: ['./drop-down-cities.component.css']
})
export class DropDownCitiesComponent implements OnInit {
  tempcities: City[];
  cities: City[]=[];
  selectedCity:any;
    @Output() cityName: EventEmitter<City> = new EventEmitter<City>();
  constructor( private cityService:cityService) { 
        this.selectedCity="Enter City";
  }

  ngOnInit() {
    console.log('cities')
    this.cityService.getCityList()
    .subscribe(response=>{this.tempcities=response;
      for(let i =0;i<this.tempcities.length;i++){
        if(this.tempcities[i].cityStatus==="Active"){
          this.cities.push(this.tempcities[i]);
        }
      }
    });
    //  console.log('selectedCity')
    //   console.log(this.selectedCity)
  }
  getSelectedCity(selectedCityname){
  
  // console.log(selectedCountryname)
  this.cityName.emit(selectedCityname);
}
  

}
