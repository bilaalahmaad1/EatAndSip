import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropDownCitiesComponent } from './common/drop-down-cities/drop-down-cities.component';
// import { NavigationComponent } from './navigation/navigation.component';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { DropDownCoutriesComponent } from './common/drop-down-coutries/drop-down-coutries.component';
import {DropDownCategoriesComponent} from './common/drop-down-categories/drop-down-categories.component';
import { DropDownCusinesComponent } from './common/drop-down-cusines/drop-down-cusines.component';
import { DropDownSubCategoriesComponent } from './common/drop-down-sub-categories/drop-down-sub-categories.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpModule,
    FormsModule,

  ],
  declarations: [DropDownCitiesComponent, DropDownCoutriesComponent,DropDownCategoriesComponent, DropDownCusinesComponent, DropDownSubCategoriesComponent],
  exports: [DropDownCitiesComponent, DropDownCoutriesComponent,DropDownCategoriesComponent,DropDownCusinesComponent,DropDownSubCategoriesComponent]
})
export class SharedModule { }
